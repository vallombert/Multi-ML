(**Constraints definitions*)

open Types

(* (\**Locality constructor*\) *)
(* val mk_bad_locality : Types.effect -> Types.effect -> Location.t -> exn *)

(**Instance of a type*)
type fragment =
  {
    tau : exprtype; (**The type*)
    effect : effect; (**The effect of the type*)
    e_cstr : e_constraints list;
    fexp_loc : Location.t; (**The location corresponding to the source file*)
  }

(**Instance of a location*)
and chunk = 
  {
    loc : effect; (**The location constraint*)
    cexp_loc : Location.t; (**The location corresponding to the source file*)
  }

(**Constraints*)
and constraints =
| CEmpty
| CTrue
| CVar of fragment
| CType of fragment
| CConst of fragment
| COp of fragment
| CPair of constraints * constraints
| CSubtype of fragment * fragment (*exprtype equality*)
| CSubtype_type of fragment * fragment (*exprtype.tau equality (no locality constraint)*)
| CEqual of fragment * fragment  (*exprtype equality*)
| CEqual_type of fragment * fragment (*exprtype.tau equality (no locality constraint)*)
| CExists of fragment * constraints
| CForall of fragment * constraints
| CAnd of constraints * constraints
| CDef of fragment * constraints * constraints
| CLet of fragment * constraints * constraints
| CTypescheme of constraints list * fragment
| CMax of chunk * chunk * chunk
| CAcceptloc_e of chunk * chunk
| CAcceptloc_t of fragment * chunk
| CAcceptdef_e of chunk * chunk
| CEqual_e of chunk * chunk
| CSeria of exprtype
(*Multi-Ml*)

(**Constraints for effects*)
and e_constraints =
| ECEmpty
(*| ECAccept of chunk * chunk
| ECAccept_t of exprtype * chunk*)
| ECTest
| ECAcceptloc of chunk * chunk
| ECAcceptloc_t of exprtype * chunk
| ECAcceptdef of chunk * chunk
| ECMax of chunk * chunk * chunk
| ECEqual of chunk * chunk
| ECSeria of exprtype
    
(**Constraints for types*)
and t_constraints =
| TCEmpty
| TCEqual of fragment * fragment
| TCEqual_type of fragment * fragment


(**Fragment constructor*)
val build_fragment : exprtype -> effect -> Location.t -> fragment

(**Chunk constructor*)
val build_chunk : effect -> Location.t -> chunk

(**Fragment to string operator*)
val fragment_to_string : fragment -> string

(**Chunk to string operator*)
val chunk_to_string : chunk -> string

(**Constraint to string operator*)
val constraints_to_string : constraints -> string

(**Constraint for effect to string operator*)
val e_constraints_to_string : e_constraints -> string

(**Generate a free variable from a given expression*)
val make_it_free : expression -> expression

(**Generate the constaint of a given expression*)
val generate_constraints : expression -> fragment -> Types.effect -> constraints * Types.effect

(**Generate a new variable identifier*)
val new_var : unit -> string

(**Generate a new locality identifier*)
val new_level : unit -> string
