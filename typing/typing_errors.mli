(**Typing error handling*)

(**Exception when a value is unbound*)
exception Unbound_Value of string * Location.t

(**Exception when a locality is not acceptable*)
exception Wrong_Locality of Types.effect * Types.effect * Location.t

(**Exception thrown when the unification of terms is impossible*)
exception Wrong_Type of Types.exprtype * Types.exprtype * Location.t

(**Exception thrown when a parallel vector imbrication is detected*)
exception Vector_Imbrication of Location.t

(**Exception thrown when a noncommunicable type is serialised *)
exception Non_Communicable of Types.exprtype * Location.t

(**Exception thrown when the unification between two localities is not acceptable *)
exception Loc_unification of Types.effect * Types.effect * Location.t
                                                 
val handle_unbounded_values : string -> Location.t -> unit

val handle_wrong_type : Types.exprtype -> Types.exprtype -> Location.t -> unit

val handle_wrong_locality : Types.effect -> Types.effect -> Location.t -> unit

val handle_vector_imbrication :  Location.t -> unit

val handle_non_communicable : Types.exprtype -> Location.t -> unit

val handle_loc_unification : Types.effect -> Types.effect -> Location.t -> unit
