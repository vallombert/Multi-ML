(**Locality unification algoritm*)

(**Unify localities*)
(* val unify_locality : Constraints.e_constraints list -> Constraints.t_constraints list -> (Constraints.chunk * Constraints.chunk) list *)
val unify_locality : Constraints.e_constraints list -> Constraints.t_constraints list -> Constraints.fragment -> Constraints.fragment
                                                                                                                   
val seria : Constraints.fragment -> unit

val unsolved_constraints : Constraints.e_constraints list ref
