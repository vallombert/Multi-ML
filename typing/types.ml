type exprtype =
| Type_base of string * effect
| Type_var of string * effect
(* | Type_scheme of string *)
| Type_arrow of (exprtype * effect * exprtype) * effect
| Type_pair of (exprtype * exprtype ) * effect
(*Multi-Ml*)
| Type_par of exprtype * effect
| Type_tree of exprtype * effect

and effect =
| Delta
| Seq
| Local
| Comm
| Bsp
| Multi
| Level of string
| Dot

let rec eq_exprtype_t e1 e2 =
  match (e1,e2) with
  | (Type_base(b1,_),Type_base(b2,_)) when b1 = b2 -> true
  | (Type_base(_,_),Type_base(_,_)) -> false
  | (Type_var(v1,_),Type_var(v2,_)) when v1 = v2 -> true
  | (Type_var(_,_),Type_var(_,_)) -> false
  | (Type_pair((p11,p12),_),Type_pair((p21,p22),_)) -> (eq_exprtype_t p11 p21) && (eq_exprtype_t p12 p22)
  | (_,Type_pair(_,_)) -> failwith "yolo"
  | _ -> false

let effect_to_string eff =
  match eff with
  | Delta -> "δ"
  | Seq -> "s"
  | Local -> "l"
  | Comm -> "c"
  | Bsp -> "b"
  | Multi -> "m"
  | Level s -> s
  | Dot -> "."

let rec exprtype_to_string expr =
  match expr with
  | Type_base (s,e) -> s^"_"^(effect_to_string e)
  | Type_var (s,e) -> s^"_"^(effect_to_string e)
  (* | Type_scheme s -> "⦇"^s^"⦈" *)
  | Type_arrow ((e1,ef,e2),e) -> "("^(exprtype_to_string e1)^" -("^(effect_to_string ef)^")-> "^(exprtype_to_string e2)^")"^"_"^(effect_to_string e)
  | Type_pair ((e1,e2),e) -> "("^(exprtype_to_string e1)^" * "^(exprtype_to_string e2)^")"^"_"^(effect_to_string e)
  | Type_par (e1,e2) -> (exprtype_to_string e1)^" par_"^(effect_to_string e2)
  | Type_tree (e1,e2) -> (exprtype_to_string e1)^" tree_"^(effect_to_string e2)

let exprtype_to_string_no_effect expr =
  match expr with
  | Type_base (s,_) -> s
  | Type_var (s,_) -> s
  (* | Type_scheme s -> "⦇"^s^"⦈" *)
  | Type_arrow ((e1,_,e2),_) -> "("^(exprtype_to_string e1)^" -> "^(exprtype_to_string e2)^")"
  | Type_pair ((e1,e2),_) -> "("^(exprtype_to_string e1)^" * "^(exprtype_to_string e2)^")"
  | Type_par (e1,_) -> (exprtype_to_string e1)^" par"
  | Type_tree _ -> failwith "TODO Tree to string"

let effect_to_string_full eff =
  match eff with
  | Delta -> "Delta"
  | Comm -> "Comm"
  | Seq -> "Sequetial"
  | Local -> "Local"
  | Bsp -> "BSP"
  | Multi -> "Multi"
  | Dot -> "."
  | Level l -> l

type expression =
  {
    expr: expression_t;
    p_loc: Location.t;
  }
and  expression_t =
  Var of string
| Const of int
| String of string
| Bool of bool
| Unit
| Op of string
| Ifthenelse of expression * expression * expression
| Fun of string * expression
| App of expression * expression
| Pair of expression * expression
| Let of string * expression * expression
| Letrec of string * string * expression * expression
(*Multi-ML*)
| Replicate of expression
| Proj of expression
| Open of string
| Copy of string
| Letmultifun of string * string * expression * expression
| Letmultitreefun of string * string * expression * expression
| Wherebody of expression * expression
| Mktree of expression
| At of string
| Multifun of string * expression

let rec expression_t_to_string expr =
  match expr with
    Var v -> v
  | Const c -> string_of_int c
  | String s -> "\""^s^"\""
  | Bool b -> if b then "true" else "false"
  | Op _ -> "OP"
  | Ifthenelse (c,e1,e2) -> "if ("^(expression_t_to_string c.expr)^") then "^(expression_t_to_string e1.expr)^" else "^(expression_t_to_string e2.expr)
  | Fun (s,e) -> "fun "^s^" -> "^(expression_t_to_string e.expr)
  | App (e1,e2) -> (expression_t_to_string e1.expr)^" "^(expression_t_to_string e2.expr)
  | Pair (e1,e2) -> "("^(expression_t_to_string e1.expr)^","^(expression_t_to_string e2.expr)^")"
  | Let (s,e1,e2) -> "let "^s^" = "^(expression_t_to_string e1.expr)^" in "^(expression_t_to_string e2.expr)
  | Letrec (s,x,e1,e2) -> "let rec "^s^" "^x^" = "^(expression_t_to_string e1.expr)^" in "^(expression_t_to_string e2.expr)
  | Replicate e -> "<< "^(expression_t_to_string e.expr)^" >>"
  | Proj e -> "Proj "^(expression_t_to_string e.expr)
  | Open s -> "$"^s^"$"
  | Copy s -> "#"^s^"#"
  | Letmultifun (f,x,e1,e2) -> "multi "^f^" "^x^" -> "^(expression_t_to_string e1.expr)^" \nin "^(expression_t_to_string e2.expr)
  | Letmultitreefun (f,x,e1,e2) -> "multi tree "^f^" "^x^" -> "^(expression_t_to_string e1.expr)^" \nin "^(expression_t_to_string e2.expr)
  | Wherebody (node,leaf) -> "\nwhere node =\n"^(expression_t_to_string node.expr)^"\nwhere leaf =\n"^(expression_t_to_string leaf.expr)
  | Mktree e -> "mktree "^(expression_t_to_string e.expr)^" "
  | At s -> "at"^s


let mk_expression e l= {expr=e;p_loc=l}

let rec type_of_exprtype e =
  match e with
  | Type_base (t,_) -> t
  | Type_var (t,_) -> t
  | Type_par(t,_) -> type_of_exprtype t
  | Type_arrow((t1,_,t2),_) -> (*C'est un peu bizare comme truc …*) (type_of_exprtype t1)^(type_of_exprtype t2)
  | Type_tree(t,_) -> type_of_exprtype t
  | _ -> failwith "Todo : type_of_exprtype"
    
let loc_of_exprtype e =
  match e with
  | Type_base (_,l) -> l
  | Type_var (_,l) -> l
  | Type_par _ -> Bsp
  | Type_arrow (_,l) -> l
  | Type_pair (_,l) -> l
  | Type_tree (_,l) -> l

let get_arrow_effect = function 
  | Type_arrow ((_,e,_),_) ->e
  | _ -> Dot

let change_loc_of_exprtype e l =
  match e with
  | Type_base (s,_) -> Type_base (s,l)
  | Type_var (s,_) -> Type_var (s,l)
  | Type_arrow ((s1,ef,s2),_) -> Type_arrow ((s1,ef,s2),l)
  | Type_par (s,_) -> Type_par (s,l)
  | Type_pair ((p1,p2),_) -> Type_pair((p1,p2),l)
  | Type_tree (s,_) -> Type_tree (s,l)
