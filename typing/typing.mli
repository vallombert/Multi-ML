(**Transforme generated types variable into "à la OCaml" types*)
val abcdize : Types.exprtype -> (Types.exprtype * ((string * string) list *(Types.effect * Types.effect) list))
                                  
(**Test typing … -- TESTING --*)
val pexp_expr_to_expression_t : Parsetree.structure -> Types.expression list
val type_expression_t : Types.expression -> Types.exprtype
