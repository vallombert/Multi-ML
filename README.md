

# Multi-ML

**Multi-ML : Programming Multi-BSP Algorithms in ML**

## What is Multi-ML?


Multi-ML is a programming language implemented over OCaml that combines the high degree of abstraction of ML with the scalable and predictable performances of multi-BSP. The multi-BSP model is an extension of the well-known BSP bridging model. It brings a tree-based view of nested components to represent hierarchical architectures. In the past, we designed BSML for programming BSP algorithms in ML. We now propose the Multi-ML language as an extension of BSML for programming Multi-BSP algorithms in ML. 

More information about the language can be found here:
https://www.lacl.fr/~vallombert/articles/Victor_Allombert_Thesis.pdf

## Compile and install the Multi-ML language

First, run the configuration tools:
`$./configure --bindir path_to_bin --libdir path_to_lib`

To build and install Multi-ML:
`$ make install`

To build the documentation :
`$ make doc`

Then, the compilers can be found in `path_to_bin`

> **Requierd packages are :**

> - OCaml 4.02.1
> - MPI (mpich or openmpi)

## Compile a Multi-ML code


To compile a Multi-ML code for a parallel machine:
`usage: mmlopt.mpi [OPTIONS] file.mml
Warning: It is possible to compile only one .mml file ! (use --lib option)
OPTIONS:
   -d: keep intermediate files for debug
   -o [file]: executable name
   -l|--lib [file|"files"]: for library
   --ccopt [option|"options"]: for OCaml compiler option
   -I [dircetory]: for each library path
   -h|--help: to get help !
`

## Run a Multi-ML code


To run a Multi-ML code for a parallel machine:
`mpirun -np $NPROCS ./exec`

## Code examples
Several code examples can be found in the following repository:
https://git.lacl.fr/vallombert/Multi-ML-Bench

