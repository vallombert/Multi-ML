(** Interface of all the main components of Multi-ML.
    @author Victor Allombert
*)


(** Interface of the modules implementing the Multi-ML primitives *)
module type MULTIML =
  sig

    (** Abstract type of a parallel vector *)
    type 'a par

    (** Abstract type of a tree*)
    type 'a tree

    (** Returns the Global IDentifier of a component*)
    val mbsp_gid : unit -> int list

    (** Returns the Processor IDentifier (process id)*)
    val mbsp_pid : unit -> int

    (** Returns a fresh dynamic identifier*)
    val generate_dynamic_id : unit -> int

    (** Returns a fresh static identifier*)
    val generate_static_id : unit -> int

    (** Raised when the requested identifier does not correspond to existing an unit (not in 0 to nb_children)*)
    exception Invalid_processor of int


    (** Parallel vector creation. 
     @return the parallel vector with the given value replicated on each children of a node.*)
    val replicate : 'a -> 'a par

    (** Parallel vector creation with local initialisation. {b Parameters :}
      - [f] function to evaluate in parallel
      Computes the value of [f] applied on each children sid and @return the corresponding vector:
      <[f 0], ..., [f (p-1)]> *)
    val mkpar : (int -> 'a) -> 'a par

    (** Projection (dual of [mkpar]/[Replicate]). Makes all the elements of a parallel vector available in the memory of the calling node. {b Parameters :}
      - [v] a parallel vector <v{_0}, ..., v{_p-1}>
      @return a function f such that f i = v{_i} *)                            
    val proj : 'a par -> int -> 'a

    (** Global communication. {b Parameters :}
      - [f] = <f{_0}, ..., f{_p-1}>, f{_i} j is the value that processor [i] should send to processor [j].
      @return a parallel vector [g] = <g{_0}, ..., g{_p-1}> where g{_j} i = f{_i}
      j is the value received by processor [j] from processor [i].
     *)
    val put : (int -> 'a) par -> (int -> 'a) par

    (** Abstract a value that is used to build a [tree]*)
    type 'a final

    (** Returns a value that may be used to build a [tree]*)
    val final_value : 'a -> 'b -> 'a

    (** Starts a timer*)
    val start_timing : unit -> unit

    (** Stops a timer*)
    val stop_timing : unit -> unit

    (** returns a parallel vector which contains, at each processor, the time
      elapsed between the calls to [start_timing] and [stop_timing].
      @raise Timer_failure if the call to one of those functions was meaningless
      ({e e.g.} [stop_timing] called before [start_timing]). *)
    val get_cost : unit -> float
    
  end

(** Machine parameters from a given the configuration file representing a Multi-BSP architecture *)
module type MACHINE_PARAMETERS =
  sig
    type 'a tree

    (** Contains the Multi-BSP architecture*)
    val architecture : (int * string * int * int * int * int) tree

    (** Contains the Multi-BSP architecture as an array*)
    val architecture_array : (string * int * int * int * int) array

    (** Prints the Multi-BSP architecture*)
    val print_architecture : (string * int * int * int * int) array -> unit

    (** Returns the color of the current component*)
    val get_level_color : int -> int

    (** Returns the color of the children of the current component*)
    val get_children_color : int -> int

    (** Returns the color of the parent of the current component*)
    val get_parent_color : int -> int

    (** Returns the color of the siblings of tre current component*)
    val get_siblings_color : int -> int

    (** Returns the type of the current memory (Shared or Distributed)*)
    val get_memory_color : int -> int
  end

(** Module providing the implementation of the communication routines*)    
module type COMM =
  sig
    (*User primitives*)
    
    (** Returns the GID of the host processor*)
    val gid : unit -> int list

    (** Returns MPI identifier of the host processor*)
    val pid : unit -> int

    (** Returns the sibling identifier of the host processor*)
    val sid : unit -> int

    (** Returns the total number of siblings of the host processor*)
    val nb_siblings : unit -> int

    (** Returns the number of children of the host processor*)
    val nb_children : unit -> int

    (** Returns the list of children of the host processor*)
    val my_children : unit -> int list

    (** Performs implementation-dependent initialization. *)
    val initialize : unit -> unit

    (** Performs implementation-dependent finalization. *)                               
    val finalize : unit -> unit

    (** Returns the name of the host*)
    val hostname : unit -> string

    (** Returns whether or not the component is a node*)
    val node : unit -> bool

    (** Returns whether or not the component the (Multi-BSP speaking) root*)
    val root : unit -> bool

    (** Performs a all to all communication among siblings*)
    val send : 'a array -> 'a array

    (** Sends [Bytes] to the i-st child of a node*)
    val send_bytes_to_child_i : bytes -> int -> unit

    (** Sends [Bytes] to all children of a node *)
    val send_bytes_to_children : bytes -> unit

    (** Sends [Integer] to all children of a node*)
    val send_int_to_children : int -> unit

    (** Receives [Bytes] from child with sid=0*)                                        
    val recv_bytes_from_child_0 : unit -> bytes

    (** Gathers [Bytes] from all children *)
    val gather_bytes_from_children : bytes -> int array -> unit

    (** Gather [Integer] from all children*)
    val gather_int_from_children : int array -> unit

    (** Performs a serialisation*)                                                  
    val marshal : 'a -> bytes

    (** Performs a un-serialisation*)
    val unmarshal : bytes -> int -> 'a

    (** Performs a global barrier (concerning the whole architecture)*)
    val mbsp_barrier : unit -> unit

    (** Performs a local barrier with the children of a node*)
    val children_barrier : unit -> unit

    (** Performs a local barrier with the parent of a node/leaf*)
    val parent_barrier : unit -> unit

    (** Returns the wall clock time value*)
    val wtime : unit -> float
  end
