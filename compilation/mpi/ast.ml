open Parsetree
open Tools
open Asttypes

let debug = ref false

let debug s = if !debug = true then Printf.printf "%s%!" (s^"\n") 

(*Générateur de noms de variables fraîches*)
(* let var_name = ref 0 *)

(* let fresh cpt = *)
(*   let v = !cpt in *)
(*   cpt := !cpt + 1; *)
(*   "gen__"^(string_of_int v);; *)


(*Table of multi functions (multi function name, 0:multi|1:multi tree)*)
type multi_spec =
  | Multi of int
  | Multi_tree of int

let multi_table : (string,multi_spec) Hashtbl.t ref = ref (Hashtbl.create 1000)
let add_to_multi_table k v =
  Hashtbl.replace !multi_table k v
                  
let find_in_multi_table k =
  try
    Hashtbl.find !multi_table k
  with
  | Not_found -> debug ("find_in_multi_table "^k^" Not Found !");failwith "Not_found"
                                 
let is_in_multi_table k =
  Hashtbl.mem !multi_table k

let incr_in_multi_table k =
  if is_in_multi_table k then
    let v =
      (function | Multi i -> Multi (i+1) | Multi_tree i -> (Multi_tree (i+1))) (find_in_multi_table k) in
    Hashtbl.replace !multi_table k v

(*Def runtime tools*)
let __multi_call = ref "__multi_call"
let __multi_tree_call = ref "__multi_tree_call"

(*Mktree cpt*)
let mktree_cpt = ref 0
let mktree_cpt_get_up () =
  let res = !mktree_cpt in
  incr mktree_cpt;res
                            
(*flag with explicite name*)
let within_vector = ref false
                       
(*Lancement des démons*)
let need_daemons = ref false


(*Multi fonction courante*)
let current_mf = ref ""


(*Initialisation list of ids*)
let init_multi_exp_lst = ref [];;
  
(*Transforme une string en expression*)
let str_to_pexp str = 
  let lexbuf = Lexing.from_string str in 
  try
    let res = Parse.implementation lexbuf in
    let pstr = (List.hd res).pstr_desc in 
    begin 
      match pstr with 
      |	Pstr_eval (e,_) -> e
      | Pstr_value (_,bl) -> (List.hd bl).pvb_expr
      (*| Pstr_value (rf,pel) -> snd (List.hd pel)*)
      | _ -> failwith("TODO : str_to_pexp")
    end
  with
  | e ->
     failwith ((Printexc.to_string e)^" : in str_to_pexp while parsing: "^str)

(*Transforme un expression en string*)
let pexp_to_str pe =
  let fe = mk_exp pe (fake_location) in
  Pprintast_mpi.string_of_expression fe


let is_let_multi e =
  begin
    match e.pexp_desc with 
    | Pexp_let (Multirecursive,v,_) -> 
       let name = ppat_var_to_str ((List.hd v).pvb_pat) in (true,Some name)
    | Pexp_let (Multitreerecursive,v,_) -> 
       let name = ppat_var_to_str ((List.hd v).pvb_pat) in (true,Some name)
    | Pexp_multi_function _ -> (true,None)
    | Pexp_multitree_function _ -> (true,None)                         
    | _ -> (false,None)
  end

let is_multi_tree e =
    begin
    match e.pexp_desc with 
    | Pexp_let (Multitreerecursive,_,_) -> true
    | Pexp_multitree_function _ -> true
    | _ -> false
  end

                                     
(*Génération des arbres*)
let init_multi_exp (expr: Parsetree.expression) (str:string) (loc:Location.t) =
  let pat_id = mk_patt (mk_ppat_var ("__"^str^"_id") (fake_location)) fake_location in
  let id = str_to_pexp ("let "^str^"_id = generate_dynamic_id ()") in
  let pat_keep = mk_patt (mk_ppat_var ("__"^str^"_tree_id") (fake_location)) fake_location in
  let keep = str_to_pexp ("let "^str^"_keep = generate_tree_id ()") in
  let id_pvt = mk_pvt pat_id id [] loc in
  let keep_pvt = mk_pvt pat_keep keep [] loc in
  let id_code = [{pstr_desc= Pstr_value(Nonrecursive,[id_pvt]);pstr_loc=loc}] in
  let keep_code =
    if is_multi_tree expr then
      [{pstr_desc= Pstr_value(Nonrecursive,[keep_pvt]);pstr_loc=loc;}]
    else []
  in
  init_multi_exp_lst := (!init_multi_exp_lst)@(id_code@keep_code)

(*Application récursive d'une fonction sur tous les éléments d'une expression*)
let rec apply_through exp f =
  begin
    match exp.pexp_desc with
    (*Core ML*)
    | Pexp_let (_,vbl,ein) ->
       let let_bindings = List.map (fun x -> x.pvb_expr) vbl in
       let res = List.concat (List.map (fun x -> apply_through x f) let_bindings) in
       res@(apply_through ein f)
    | Pexp_apply (a1,a2) ->
       (apply_through a1 f)@(List.flatten (List.map (fun x ->(apply_through (snd x) f)) a2))
    | Pexp_fun (_,_,_,e) ->
       apply_through e f
    | Pexp_ident _ ->
       [f exp]
    | Pexp_ifthenelse (e1,e2,e3) ->
       let e3 = 
	 begin
	   match e3 with
	   | Some e -> (apply_through e f)
	   | None -> []
	 end
       in
       (apply_through e1 f)@(apply_through e2 f)@(e3)
    | Pexp_constant _ ->
       [f exp]
    | Pexp_construct _ ->
       [f exp]
    | Pexp_array _ ->
       [f exp]
    | Pexp_tuple (e1) ->
       List.concat( List.map (fun x -> apply_through x f) e1)
    (*Multi-ML*)
    | Pexp_copy _ ->
       [f exp]
    | Pexp_open_vector _ ->
       [f exp]
    | Pexp_pid _ ->
       [f exp]       
    | Pexp_sid _ ->
       [f exp]
    | Pexp_nbsiblings _ ->
       [f exp]
    | Pexp_at _ ->
       [f exp]
    | Pexp_for (_,e1,e2,_,e3) ->
       (apply_through e1 f)@(apply_through e2 f)@(apply_through e3 f)
    | Pexp_sequence (e1,e2) ->
       (apply_through e1 f)@(apply_through e2 f)
    | Pexp_match (e,case_list) ->
       let res = List.concat (List.map (fun x -> apply_through (x.pc_rhs) f) case_list) in
       (apply_through e f)@res
    | e ->
       Printf.printf "Failed (while searching for $, # and multi-function) on %s\n" (pexp_to_str e);
       failwith "Match fail: apply_through"
  end

(*Réécriture de l'ast*)
(*Pexp_X actuellement gérées :
  -Multi ML : Pexp_multi_function;Pexp_wherebody;Pexp_finally
  -BSML : Pexp_replicate;Pexp_mkpar;Pexp_proj;Pexp_put;Pexp_open_vector;Pexp_copy;
  -Ocaml : Pexp_let;Pexp_fun;Pexp_apply;Pexp_ident;Pexp_tuple;Pexp_sequence;Pexp_ifthenelse;Pexp_constant;Pexp_construct;Pexp_array;Pexp_match;Pexp_function;Pexp_mktree;
  À enrichir ;)
*)
let rec texpr in_ast = 
  let res = 
    begin
      match in_ast.pexp_desc with 
      (*Multi-ML*)      

      | Pexp_multi_function (_,pe) ->
          debug "Pexp_multi_function";
          let nb_args = ref 0 in
	  (*Decompose the multi function*)
	  let rec unwrap_multi arg lst =
	    begin
	      match (fun x -> x.pexp_desc ) (snd (List.hd arg)) with 
	      (*Gestion des arguments*)
              | Pexp_multi_function (_,pe) ->
                  let _ = incr_in_multi_table !current_mf in
	          let new_patt = mk_patt (mk_ppat_var (ppat_var_to_str (fst (List.hd arg))) fake_location) fake_location in
	          let res = unwrap_multi pe (lst@[(ppat_var_to_str (fst (List.hd arg)))]) in
	          mk_exp
		    (Pexp_fun(
		        "",
		        None,
		        new_patt,
		        res))
		    fake_location
	      (*Transformation des codes nodes/leaf*)
	      | Pexp_wherebody (e1,e2) ->
                  debug "Pexp_wherebody";
	          let new_cond = str_to_pexp "node ()" in
	          let new_if = texpr e1 in
	          let new_else = Some (texpr e2) in
	          (*If node else leaf*)
	          let cond = mk_exp (Pexp_ifthenelse(new_cond,new_if,new_else)) fake_location in
                  let new_patt = mk_patt (mk_ppat_var (ppat_var_to_str (fst (List.hd arg))) fake_location) fake_location in
	          mk_exp
		    (Pexp_fun(
		        "",
		        None,
		        new_patt,
		        cond))
		    fake_location

	      | _ -> failwith "Match fail : unwrap_multi"

     end
	  in
	  let final_fun = unwrap_multi pe [] in
	  final_fun.pexp_desc

      | Pexp_multitree_function (_,pe) ->
          debug "Pexp_multitree_function"; 
	  (*Decompose the multi tree function*)
	  let rec unwrap_multi arg lst =
	    begin
	      match (fun x -> x.pexp_desc ) (snd (List.hd arg)) with 
	      (*Arguments*)
              | Pexp_multitree_function (_,pe) ->
                  let _ = incr_in_multi_table !current_mf in
	          let new_patt = mk_patt (mk_ppat_var (ppat_var_to_str (fst (List.hd arg))) fake_location) fake_location in
	          let res = unwrap_multi pe (lst@[(ppat_var_to_str (fst (List.hd arg)))]) in
	          mk_exp
		    (Pexp_fun(
		        "",
		        None,
		        new_patt,
		        res))
		    fake_location
	      (*Transformation des codes nodes/leaf*)
	      | Pexp_wherebodytree (e1,e2) ->
                  debug "Pexp_wherebodytree";
	          let new_cond = str_to_pexp "node ()" in
                  (*Add «update reccall vector» id to new if*)


	          let new_if = mk_exp( Pexp_sequence(
                      (str_to_pexp ("update_rccvid ()")),
                      (texpr e1)))
                      (in_ast.pexp_loc) in
	          let new_else = Some (texpr e2) in
	          (*If node else leaf*)
	          let cond = Pexp_ifthenelse(new_cond,new_if,new_else) in
                  let cond' = str_to_pexp ("("^(pexp_to_str cond)^")()") in
                  let new_patt = mk_patt (mk_ppat_var (ppat_var_to_str (fst (List.hd arg))) fake_location) fake_location in
	          mk_exp
		    (Pexp_fun(
		       "",
		       None,
		       new_patt,
		       cond'))
		  fake_location

	     | _ -> failwith "Match fail : unwrap_multi"

	   end
	 in
	 let final_fun = unwrap_multi pe [] in
	 final_fun.pexp_desc

      | Pexp_mktree e ->
         debug "Pexp_mktree";
         (*building mktree tree id*)
         let new_id = mktree_cpt_get_up () in
         let loc = fake_location in
         let str = ("__mktree_"^string_of_int new_id ^"_id_tree") in
         let pat_id = mk_patt (mk_ppat_var str (fake_location)) fake_location in
         let id = str_to_pexp ("let "^str^" = generate_dynamic_id ()") in
         let id_pvt = mk_pvt pat_id id [] loc in
         let id_code = [{pstr_desc= Pstr_value(Nonrecursive,[id_pvt]);pstr_loc=loc}] in
         init_multi_exp_lst := (!init_multi_exp_lst)@(id_code);
         let e_exp = texpr e in

         let final_exp = str_to_pexp ("((add_to_tree_tbl "^(str)^" (fun _ -> Obj.magic ("^(pexp_to_str e_exp.pexp_desc)^")));"^str^")") in
         
         final_exp.pexp_desc
           
      (*BSML*)
      | Pexp_replicate e ->
         debug "Pexp_replicate";
	 (*Generate sub expression*)
         within_vector := true;
	 let texpr_res = texpr e in
         within_vector := false;
	 (*Générartion des arguments de la fonction*)
	 let rec farg_replicate e =
	   let remove_blank l =
	     List.flatten (List.map (fun x -> if x = "" then [] else [x]) l) in
           let rec build_replicate_fun =
             (fun x ->
	       match x.pexp_desc with  
	       | Pexp_ident id ->
		  if is_pervasive_op (lident_to_string (id.txt)) then
		    ("","") (*TODO ne pas communiquer ce genre de choses pour une version opt*)
		  else (
		    try
		      begin
		        match find_in_multi_table (lident_to_string id.txt) with
		        (*C'est une multi fonction -> add to args*)
		        | _ -> 
			   debug "Call récursif trouvé dans le replicate !";
			   ((lident_to_string id.txt),"(Obj.magic)__"^(lident_to_string id.txt)^"_id;")
		      end
		    with
		    (*Pas multi fonction, on ne fait rien*)
		    | _ -> ("","")
		  )
	       | Pexp_open_vector id -> ((lident_to_string (id.txt)),"(Obj.magic)"^(lident_to_string (id.txt))^";")
	       | Pexp_copy _ -> ("","") (*Nothing to do with this implementation*)
	       | _ -> ("","")
	     ) in
           let res = (apply_through e build_replicate_fun) in
           let new_fun,new_arg = List.split res in
           let new_fun,new_arg = remove_blank new_fun,remove_blank new_arg in
           let final_fun =
             if List.length new_fun = 0 then (
	       mk_ppat_var (List.fold_right (fun x y -> x^" "^y) (["_ "]) "") fake_location)
	     else (
	       mk_ppat_var (List.fold_right (fun x y -> x^" "^y) new_fun "") fake_location
             )
           and final_arg = new_arg in
           (final_fun,final_arg)
	 in
         let final_fun,final_arg = farg_replicate e in
	 let final_fun_patt = mk_patt final_fun fake_location in
	 (*Rajout de la hmap ou pas*)
	 let final_fun_expr = 
	   mk_exp (Pexp_fun ("",None,final_fun_patt,texpr_res)) in_ast.pexp_loc 
	 in
         let final_arg_expr =
           List.fold_left (fun x y -> x^y) "" final_arg in
         let _ = (pexp_to_str final_fun_expr.pexp_desc) in
	 let final_exp = 
	   str_to_pexp 
	     ("generated_replicate ("^(pexp_to_str final_fun_expr.pexp_desc)^") [|"^(final_arg_expr)^"|]") in
	 Pexp_replicate final_exp


                       
	  
      | Pexp_mkpar e -> debug "Pexp_mkpar";
	Pexp_mkpar (texpr e)

      | Pexp_proj e -> debug "Pexp_proj";
	Pexp_proj (texpr e)

      | Pexp_put e -> debug "Pexp_put";
	Pexp_put (texpr e)
	  
      | Pexp_open_vector e -> debug "Pexp_open_vector";
	let s = lident_to_string (e.txt) in
	Pexp_open_vector e

      | Pexp_copy e -> debug "Pexp_copy";
	let s = lident_to_string (e.txt) in
	Pexp_copy e

      | Pexp_finally (e1) ->
         debug "Pexp_finally";
         let final_exp =
           begin
             match e1.pexp_desc with
             | Pexp_tuple lst ->
                if List.length lst = 1 then
                  (str_to_pexp ("__finally (None,"^pexp_to_str (List.nth lst 0).pexp_desc^") "^"__"^(!current_mf)^"_tree_id ")).pexp_desc
                else
                  (str_to_pexp ("__finally (Some "^pexp_to_str (List.nth lst 0).pexp_desc^" ,"^pexp_to_str (List.nth lst 1).pexp_desc^") "^"__"^(!current_mf)^"_tree_id ")).pexp_desc
             (*List.nth lst 0, List.nth lst 1*)
             | _ -> failwith "Pexp_finally fail : must be a pair"
           end
             
      (*let final_exp =
           (function | Some x ->
                        Pexp_tuple [mk_exp (Pexp_construct (, Some (texpr x))) (in_ast.pexp_loc);texpr e2]
                     | None ->
                        Pexp_tuple [mk_exp (Pexp_construct (, None)) (in_ast.pexp_loc);texpr e2]) e1*)
  
         in
         final_exp

      | Pexp_at e -> debug "Pexp_at";
         let id_tree = lident_to_string (e.txt) in
         let final_exp = str_to_pexp ("(Obj.magic (get_from_tree_tbl "^id_tree^"))()") in
         final_exp.pexp_desc
         
      (*Ocaml*)
      | Pexp_let (rf,vbl,e_in) ->
         debug "Pexp_let";
         (*Check if it is a let multi constructor*)
         begin
	   match rf with
	   | Multirecursive ->
              debug "Multi function !";
              let name = (ppat_var_to_str (List.hd vbl).pvb_pat) in
              add_to_multi_table name (Multi 1);
              current_mf := name;
              init_multi_exp in_ast name fake_location
           | Multitreerecursive -> 
              debug "Multi tree function !";
              let name = (ppat_var_to_str (List.hd vbl).pvb_pat) in
              add_to_multi_table name (Multi_tree 1);
              current_mf := name;
              init_multi_exp in_ast name fake_location
	   | _ -> ()
	 end;                       
	 let new_vb = List.map (fun x -> mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc) vbl in
	 let new_ein = texpr e_in in
	 let new_let = Pexp_let (rf,new_vb,new_ein) in
	 new_let

      | Pexp_fun (s,r,p,e) -> debug "Pexp_fun";
	let new_e = texpr e in
	Pexp_fun (s,r,p,new_e)

      | Pexp_function case_lst -> debug "Pexp_function";
	let new_case_lst = 
	  List.map (fun x -> 
	    {pc_lhs=x.pc_lhs;
	     pc_guard=(function Some x -> Some (texpr x) | None -> None) x.pc_guard ;
	     pc_rhs=texpr x.pc_rhs}) case_lst in
	Pexp_function new_case_lst

      | Pexp_apply (a1,a2) ->
         debug "Pexp_apply";
         let new_a1 = texpr a1 in
	 let new_a2 = List.map (fun x -> (fst x, texpr (snd x))) a2 in
	 Pexp_apply (new_a1,new_a2)

      | Pexp_ident i ->
         debug "Pexp_ident";
	 begin
           (*Is it a multi function ?*)
	   if is_in_multi_table (lident_to_string i.txt) && (not !within_vector) then
	       let s = lident_to_string i.txt in
	       debug( "ident of multi_function: "^s);
	       (*Référence sur arbre pas encore implémenté *)
	       need_daemons := true;
               let add_dyn = str_to_pexp ("add_to_dyn !__htbl_did (__"^s^"_id) "^s) in
               let is_multi_tree =
                 (function | Multi_tree _ -> true | Multi _ -> false) (find_in_multi_table s) in
               let nb_args = (function | Multi i -> i| Multi_tree i -> i)(find_in_multi_table s) in
               let _ = if nb_args > 4 then failwith "Todo : handle multi-function with more than 4 arguments" in
               let multi_fun_type = (if is_multi_tree then
                                      !__multi_tree_call
                                    else
                                      !__multi_call)^"_"^(string_of_int nb_args) in
               let multi_call =
                 mk_exp (
                     Pexp_sequence(add_dyn,
                                   mk_exp (mk_pexp_ident multi_fun_type (in_ast.pexp_loc)) (in_ast.pexp_loc)))
                   (in_ast.pexp_loc)
               in
               (*Add the tree id when it is a multi tree function call*)
               let multi_tree_id =
                 if is_multi_tree then
                   [("",str_to_pexp ("__"^s^"_tree_id"))]
                 else [] in
               let current_ident = [("",mk_exp (Pexp_ident i) (in_ast.pexp_loc))]@multi_tree_id in
               (mk_exp (Pexp_apply(multi_call,current_ident)) in_ast.pexp_loc).pexp_desc
	   else
	     (debug ("ident : "^(lident_to_string i.txt)^"\n");
	      Pexp_ident i)
	 end

      | Pexp_tuple e -> debug "Pexp_tuple";
	                Pexp_tuple (List.map texpr e)

      | Pexp_sequence (e1,e2) -> debug "Pexp_sequence";
	Pexp_sequence (texpr e1,texpr e2)

      | Pexp_ifthenelse (e1,e2,eo) -> debug "Pexp_ifthenelse";
	let new_eo =
	  begin
	    match eo with 
	    | Some e -> Some (texpr e)
	    | None -> None
	  end
	in
	Pexp_ifthenelse (texpr e1, texpr e2, new_eo)

      | Pexp_constant c -> debug "Pexp_constant"; 
	Pexp_constant c

      | Pexp_construct (e,f) -> debug "Pexp_construct";
	Pexp_construct (e,f)
	  
      | Pexp_array ar -> debug "Pexp_array";
	Pexp_array ar

      | Pexp_match (e,case_lst) -> debug "Pexp_match";
	let new_case_lst = 
	  List.map (fun x -> 
	    {pc_lhs=x.pc_lhs;
	     pc_guard=(function Some x -> Some (texpr x) | None -> None) x.pc_guard ;
	     pc_rhs=texpr x.pc_rhs}) case_lst in
	Pexp_match (e,new_case_lst)

      | e -> e
      (*| e ->
         Printf.printf "%s\n" (pexp_to_str e);
         failwith("Tiens, t'as encore oublié un Pexp !! ;)")*)
    end
  in {pexp_desc=res;pexp_loc=in_ast.pexp_loc;pexp_attributes=[]}


(* let rec check_multi_fun lst =  *)
(*   begin *)
(*     match lst with *)
(*     | h::t -> *)
(*       begin  *)
(* 	match h with *)
(* 	| (p,_) -> print_endline (ppat_var_to_str p) *)
(*       end ; *)
(*       check_multi_fun t *)
(*     | [] -> () *)
(*   end *)
    
(* (\*Enrichissement de l'env avec les multi fonctions*\) *)
(* let rec add_id_to_multi exp = *)
(*   begin *)
(*     match exp.pexp_desc with *)
(*     | Pexp_multi_function (l,pel) -> *)
(*       Printf.printf "%%%%%% found one !\n"; *)
(*       let res = add_id_to_multi (snd (List.hd pel)) in *)
(*       mk_exp (Pexp_multi_function (l,[(fst (List.hd pel)),res])) fake_location *)
(*     | _ ->  *)
(*       let patt_ht = mk_patt (mk_ppat_var "__id" fake_location) fake_location in *)
(*       mk_exp (Pexp_multi_function ("",[patt_ht,exp])) fake_location *)
(*   end *)

(*Ajouter les démons pour le runtime*)
let wrap_daemon exp =
  (*if !need_daemons then (
    let str = "if pid () = 0 then ("^(pexp_to_str (exp.pexp_desc))^" ;()) else daemon ()" in
    need_daemons := false; str_to_pexp str)
  else exp*)
  exp

(*Transformation des pstr de l'ast*)
let run_transform a =
  begin
    match a.pstr_desc with
    | Pstr_value (rf,vbl) ->
       debug "\nPstr_value";
       let pel = (List.map (fun x -> (x.pvb_pat,x.pvb_expr)) vbl) in
       let expr = (snd (List.hd pel)) in
       let is_mf,name_option = is_let_multi expr in
       let name =
         (function | None -> ppat_var_to_str (fst (List.hd pel))
                   | Some _ -> ppat_var_to_str (fst (List.hd pel)) ) name_option
      (* ppat_var_to_str (fst (List.hd pel)) *) in
      if is_mf then (
	debug ("Multi binding = "^name);
        (*is it a multi_tree_function*)
        let multi_cstr = (function |true -> Multi_tree 1 | false -> Multi 1) (is_multi_tree expr) in
	current_mf := name;
        add_to_multi_table name multi_cstr;
	let _  = init_multi_exp (snd (List.hd pel)) name (a.pstr_loc) in
        let res = List.map (fun (x) -> mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc ) vbl in
	current_mf := "";
        [{pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;}]
      )
      else(
	debug ("Function binding ="^name);
	  (* let res = List.map (fun (x) ->  *)
	  (*   mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc  *)
	  (* ) vbl in *)
	  (* let f_exp = [{pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;}] in *)
	if !need_daemons then
	  let res = List.map (fun (x) -> 
	    mk_pvt x.pvb_pat (wrap_daemon (texpr x.pvb_expr)) x.pvb_attributes x.pvb_loc 
	  ) vbl in
	  [{pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;}]
	else (
	  let res = List.map (fun (x) -> 
	                mk_pvt x.pvb_pat (texpr x.pvb_expr) x.pvb_attributes x.pvb_loc 
	              ) vbl in
	  [{pstr_desc= Pstr_value(rf,res);pstr_loc=a.pstr_loc;}])
      )
	    
	    
    | Pstr_eval (e,attr) ->
       debug "\nPstr_eval";
       let res = texpr e in 
       let is_mf = is_let_multi (e) in
       if fst is_mf then (
         try
	 let _ = init_multi_exp e (unsome (snd is_mf)) (a.pstr_loc) in
           [{pstr_desc= Pstr_eval(res,attr);pstr_loc=a.pstr_loc;}]
	 with
	 | Not_found -> []
       )
       else(
	 [{pstr_desc= Pstr_eval(res,attr);pstr_loc=a.pstr_loc;}]
       )

    | p ->
       [{pstr_desc= p;pstr_loc=a.pstr_loc;}]
    (* | _ -> failwith "Match fail: run_transform" *)
  end

(*May be useful …*)
(*Chercher et remplacer les variables annotées "tree"*)
(* let seek_and_replace tbl ast = *)
(*   let flags : (string,bool) Hashtbl.t = Hashtbl.create 1000 in *)
(*   ignore (Hashtbl.iter (fun id _ -> Hashtbl.replace flags id true) tbl); *)
(*   ignore (Hashtbl.iter (fun id v -> Printf.printf "%s -> %s\n" id (if v then "true" else "false")) flags); *)
(*   (\*Fct de transformation des sids*\) *)
(*   let rec seek x = *)
(*     ignore( *)
(*     begin match x.pexp_desc with    *)
(*     | Pexp_replicate e -> debug "Pexp_replicate"; *)
(*       Pexp_replicate (seek e) *)
(*     | Pexp_mkpar e -> debug "Pexp_mkpar"; *)
(*       Pexp_mkpar(seek e) *)
(*     | Pexp_proj e -> debug "Pexp_proj"; *)
(*       Pexp_proj (seek e) *)
(*     | Pexp_put e -> debug "Pexp_put"; *)
(*       Pexp_put (seek e) *)
(*     | Pexp_open_vector e -> debug "Pexp_open_vector"; *)
(*       Pexp_open_vector e *)
(*     | Pexp_copy e -> debug "Pexp_copy"; *)
(*       Pexp_copy e *)
(*     | Pexp_let (rf,vbl,e_in) -> debug "Pexp_let"; *)
(*       let new_vb = List.map (fun x -> mk_pvt x.pvb_pat (seek x.pvb_expr) x.pvb_attributes x.pvb_loc) vbl in *)
(*       let new_ein = seek e_in in *)
(*       Pexp_let (rf,new_vb,new_ein) *)
(*     | Pexp_fun (s,r,p,e) -> debug "Pexp_fun"; *)
(*       let new_e = seek e in *)
(*       Pexp_fun (s,r,p,new_e) *)
(*     | Pexp_apply (a1,a2) ->  debug "Pexp_apply"; *)
(* 	let new_a1 = seek a1 in *)
(* 	let new_a2 = List.map (fun x -> (fst x, seek (snd x))) a2 in *)
(* 	Pexp_apply (new_a1,new_a2) *)
(*     | Pexp_ident i -> debug "Pexp_ident"; *)
(*       if (Hashtbl.mem flags (lident_to_string i.txt)) then  *)
(* 	Printf.printf "/////yeah\n"; *)
(*       Pexp_ident i *)
(*     | Pexp_tuple e -> debug "Pexp_tuple"; *)
(*       Pexp_tuple (List.map seek e) *)
(*     | Pexp_sequence (e1,e2) ->  debug "Pexp_sequence"; *)
(*       Pexp_sequence (seek e1,seek e2) *)
(*     | Pexp_ifthenelse (e1,e2,eo) ->  debug "Pexp_ifthenelse"; *)
(*       let new_eo = *)
(* 	begin *)
(* 	  match eo with  *)
(* 	  | Some e -> Some (seek e) *)
(* 	  | None -> None *)
(* 	end *)
(*       in *)
(*       Pexp_ifthenelse (seek e1, seek e2, new_eo) *)
(*     | Pexp_constant c -> debug "Pexp_constant"; *)
(*       Pexp_constant c *)
(*     | Pexp_construct (e,f) -> debug "Pexp_construct"; *)
(*       Pexp_construct (e,f) *)
(*     | Pexp_array ar -> debug "Pexp_array"; *)
(*       Pexp_array ar *)
(*     | _ -> failwith "TODO : seek_and_replace" *)
(*     end; *)
(*     ); *)
(*     x *)
(*   in *)
(*    (\*Uncap et application sur tout l'ast*\) *)
(*   let fct ast =  *)
(*     begin *)
(*       match ast.pstr_desc with *)
(*       | Pstr_eval (e,attr) -> *)
(* 	{pstr_desc= Pstr_eval (seek e,attr);pstr_loc=ast.pstr_loc;} *)
(*       | Pstr_value (rf,vbl) -> *)
(* 	(\* let pel = (List.map (fun x -> seek (x.pvb_expr)) vbl) in  *\) *)
(* 	 {pstr_desc= Pstr_value (rf,vbl);pstr_loc=ast.pstr_loc;} *)
(*       | _ -> failwith "ast_mpi match fail" *)
(*     end in *)
(*   let res = List.map (fun e -> fct e) ast in res *)


(*Lancer la transformation de l'AST complet*)
let transform in_ast = 
  let mpi_ast = List.flatten (List.map run_transform in_ast) in
  (!init_multi_exp_lst)@mpi_ast

(*Transformations sids*)
let replace_sids in_ast =
  (*let res = seek_and_replace Tools.type_tree_table in_ast in*)
  debug "Replacing static ids : C'est mort, il faut le faire à la main !!!\n"; 
  (*res*)in_ast
