include Parallel.Make(Parameters_in_file)(Mpi)

include Mpi
open Mpi

external init_archi : int -> int -> int -> int -> int -> unit = "ocaml_mpi_init_archi";;

let architecture = Parameters_in_file.architecture_array
(*let _ = if pid () = 0 then Parameters_in_file.print_architecture architecture *)

let color_s = Parameters_in_file.get_sons_color (pid())
  (* begin *)
  (*   match pid () with *)
  (*     0 -> 1 *)
  (*   | 1 -> 2 *)
  (*   | 2 -> 3 *)
  (*   | _ -> 0 *)
  (* end *)

let color_f = Parameters_in_file.get_father_color (pid())

(* let color_f = *)
(*   begin *)
(*     match pid () with *)
(*       0 -> 0 *)
(*     | 1 -> 1 *)
(*     | 2 -> 1 *)
(*     | 3 -> 2 *)
(*     | 4 -> 2 *)
(*     | 5 -> 3 *)
(*     | 6 -> 3 *)
(*   end *)


let color_b = Parameters_in_file.get_brothers_color (pid())
  (* begin *)
  (*   match pid() with *)
  (*     0 -> 1 *)
  (*   | 1 -> 2 *)
  (*   | 2 -> 2 *)
  (*   | 3 -> 3 *)
  (*   | 4 -> 3 *)
  (*   | 5 -> 4 *)
  (*   | 6 -> 4 *)
  (*   | _ -> failwith("Nope") *)
  (* end *)


let color_level = Parameters_in_file.get_level_color (pid())
  (* begin *)
  (*   match pid() with *)
  (*     0 -> 0 *)
  (*   | 1 -> 1 *)
  (*   | 2 -> 1 *)
  (*   | 3 -> 2 *)
  (*   | 4 -> 2 *)
  (*   | 5 -> 2 *)
  (*   | 6 -> 2 *)
  (*   | _ -> failwith("Nope") *)
  (* end *)

let color_memory = Parameters_in_file.get_memory_color (pid())

let _ = init_archi color_level color_f color_s color_b color_memory

(* external init_archi : int -> int -> int -> unit = "ocaml_mpi_init_archi" *)
external mpi_wait_job : unit -> bytes = "ocaml_mpi_wait_job"
external mpi_wait_args : unit -> bytes = "ocaml_mpi_wait_job"
external mpi_wait_id : unit -> int = "ocaml_mpi_wait_id"

(* let _ = init_archi color_1 color_2 color_b *)


let __htbl_did : (int, int -> int)  Hashtbl.t ref = ref (Hashtbl.create 1000)

let add_to_dyn ht id v =
  Printf.printf "Did_add : pid %i -> id %i\n" (pid()) id;
  Hashtbl.replace ht id ((Obj.magic)v)

let did_get id =
  try
    (* Printf.printf "Did_get %i\n" id; *)
    Hashtbl.find !__htbl_did id
  with
  | Not_found ->
    let v = Printf.sprintf "Error did_get : Not_found -> id = %i" id
    in Printf.printf "Failwith %s \n" v;
    (Obj.magic "")
    (* failwith v *)

let did_clean : int array -> unit =
  fun ids ->
    for i = 0 to Array.length ids do
      Hashtbl.remove !__htbl_did (ids.(i))
    done

let rec apply_args mf mtab =
  let (tab : int array) = Marshal.from_bytes mtab 0 in
  if Array.length tab = 0 then(
    (*Printf.printf "0 arg\n";*)
    let (f: unit -> 'b -> 'c ) = Marshal.from_bytes mf 0 in
    f ()
  )
  else if Array.length tab = 1 then (
    (* Printf.printf "1 arg\n"; *)
    let (f: 'a -> 'b -> 'c) = Marshal.from_bytes mf 0 in
    let v0 = (Obj.magic)(did_get (tab.(0))) in
    f v0
  ) else if Array.length tab = 2 then (
    (* Printf.printf "2 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    f v0 v1
  ) else if Array.length tab = 3 then (
    (* Printf.printf "3 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e ) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    f v0 v1 v2
  ) else if Array.length tab = 4 then (
    (* Printf.printf "4 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e -> 'f) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    let v3 = ((Obj.magic))(did_get (tab.(3))) in
    f v0 v1 v2 v3
  ) else if Array.length tab = 5 then (
    (* Printf.printf "5 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g ) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    let v3 = ((Obj.magic))(did_get (tab.(3))) in
    let v4 = ((Obj.magic))(did_get (tab.(4))) in
    f v0 v1 v2 v3 v4
  ) else if Array.length tab = 6 then (
    (* Printf.printf "6 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    let v3 = ((Obj.magic))(did_get (tab.(3))) in
    let v4 = ((Obj.magic))(did_get (tab.(4))) in
    let v5 = ((Obj.magic))(did_get (tab.(5))) in
    f v0 v1 v2 v3 v4 v5
  ) else failwith("Max args is 6 … TODO generic apply_args \n")

external mpi_send_mfun_to_sons : bytes-> unit = "ocaml_mpi_distribute_job_to_sons"
external mpi_send_mfun_to_father : bytes -> unit = "ocaml_mpi_send_mfun_father"

external sleep : int -> unit = "c_usleep"
let _ = sleep ((pid())*0)

let rec wait_for_job () =
  let mjob = mpi_wait_job () in
  begin
    match mjob with
    | "gc" -> Printf.printf "Run GC bitch !\n";wait_for_job()
    | "-1" ->
      if node () then (
	Printf.printf "\x1b[31mKill pid %i @%s\x1b[0m\n" (pid ()) (hostname ());
	mpi_send_mfun_to_sons "-1" )
      else (Printf.printf "\x1b[31mKill pid %i @%s\x1b[0m\n" (pid ()) (hostname ()))
    | "proj" ->
      let id = mpi_wait_id () in
      (* let id = generate_dynamic_id () in *)
      let vl = (Obj.magic)(did_get id) in
      (* Printf.printf "Request Proj sur l'id no %i\n" id; *)
      let data = Array.init (nb_brothers()) (fun i -> vl) in
      let send_res = send data in
      let proj_res =
	fun i ->
	  if not((0<=i) && (i<nb_sons()))
	  then raise (Invalid_processor i)
	  else
	    send_res.(i) in
      let m_proj = Marshal.to_bytes proj_res [Marshal.Closures] in
      if bid () = 0 then (
	mpi_send_mfun_to_father m_proj);
      wait_for_job()
    | "proj_opt" ->
      let id = mpi_wait_id () in
      (* let id = generate_dynamic_id () in *)
      let vl = (Obj.magic)(did_get id) in
      (* Printf.printf "Request Proj sur l'id no %i\n" id; *)
      let data = Array.init (nb_brothers()) (fun i -> vl) in
      let send_res = send data in
      let proj_res =
	fun i ->
	  if not((0<=i) && (i<nb_sons()))
	  then raise (Invalid_processor i)
	  else
	    send_res.(i) in
      let m_proj = Marshal.to_bytes proj_res [Marshal.Closures] in
      if bid () = 0 then (
	mpi_send_mfun_to_father m_proj);
      wait_for_job()
    | "put" ->
      (*Todo*)
      let id = mpi_wait_id () in
      (* let id = generate_dynamic_id () in *)
      let (vf : int -> 'a) = (Obj.magic)(did_get id) in
      let data = Array.init (nb_brothers()) (fun i -> vf i) in
      let res = send data in
      let new_id = mpi_wait_id () in
      add_to_dyn !__htbl_did (new_id) (fun i -> res.(i));
      wait_for_job()
    | _ ->
      let id = mpi_wait_id () in
      (* let id = generate_dynamic_id () in *)
      let margs = mpi_wait_args () in
      (*Printf.printf "new Binding on %i\n" id;*)
      (*Eval de ce qui est reçu*)
      let eval = apply_args mjob margs in
      (*Ajout du binding*)
      add_to_dyn !__htbl_did (id) (eval);
      (*On retourne en attente*)
      wait_for_job ()
  end

let daemon () = wait_for_job ()


let __htbl_sid : (int, int -> int)  Hashtbl.t ref = ref (Hashtbl.create 1000)

let add_to_stat id v =
  Printf.printf "Sid_add : pid %i -> id %i\n" (pid()) id;
  Hashtbl.replace !__htbl_sid id ((Obj.magic)v)

let sid_get id =
  try
    (* Printf.printf "Did_get %i\n" id; *)
    Hashtbl.find !__htbl_sid id
  with
  | Not_found ->
    let v = Printf.sprintf "Error sid_get : Not_found -> id = %i" id
    in Printf.printf "Failwith %s \n" v;
    (Obj.magic "")
    (* failwith v *)


let _serial_kill ()=
  Printf.printf "\x1b[31mKill pid %i @%s\x1b[0m\n" (pid ()) (hostname ());
  let stop = "-1" in
  mpi_send_mfun_to_sons stop

let generated_finally ~up ~keep k =
  Printf.printf "Finally : ";
  add_to_stat k keep;
  final_value up keep

(*warning !! si fonction du genre (fun i -> i ) ====> fun _ _ i -> i et pas l'inverse ;)*)
let generated_replicate f ar =
  let _tmp = replicate (f) in
  mpi_send_mfun_to_sons (Marshal.to_bytes ar [Marshal.Closures]) ;
  _tmp

let generated_mkpar f =
  let _tmp = mkpar (f) in
  mpi_send_mfun_to_sons (Marshal.to_bytes [||] [Marshal.Closures]) ;
  _tmp

let generated_proj v =
  (Obj.magic) (proj v)

let at t =
  (Obj.magic (sid_get t))
