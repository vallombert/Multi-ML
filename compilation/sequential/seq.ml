let infile = ref ""

let usage = "usage: " ^ Sys.argv.(0) ^ " [-i filename]" ^ " [options]"

let speclist = [
  ("-i", Arg.Set_string infile, ": input Multi-ML file (*.mml)");
]

let () =
  Arg.parse
    speclist
    (fun x -> raise (Arg.Bad ("Bad argument : " ^ x)))
    usage;;


let astout_multi_ml = "./multi_ml.ast"

let astout_mpi = "./multi_ml-mpi.ast"

let ast_multi_ml =
  let file = !infile in 
  let channel = open_in file in
  let lexbuf = Lexing.from_channel channel in
  try
    (*Parse file*)
    Printf.printf "Parsing … ";
    let ast_multi_ml = Parse.implementation lexbuf in
    Printf.printf "Ok\n";
    (*Write Ast*)
    let outfile = open_out astout_multi_ml in
    let out_file_formatter = Format.formatter_of_out_channel outfile in
    let _ = Printast.implementation out_file_formatter ast_multi_ml in
    flush outfile;
    close_out outfile;
    Printf.printf "Ast available in: %s\n" astout_multi_ml;
    (*Type file*)
    Printf.printf "Typing … ";
    let expr = Typing.pexp_expr_to_expression_t ast_multi_ml in
    let expr_type = Typing.type_expression_t (List. hd expr) in
    let _ = Typing.abcdize expr_type in 
    Printf.printf "ok\n";
    (*Write Mpi Ast*)
    let outfile = open_out astout_mpi in
    let out_file_formatter = Format.formatter_of_out_channel outfile in
    let ast_mpi = Ast.transform (ast_multi_ml(*@(Mpi.serial_kill())*)) in
    (* let sids = Ast.transform (Mpi.build_sids ()) in*)
    (* let ast_mpi = sids@(Ast.replace_sids ast_mpi) in *)
    let _ = Printast.implementation out_file_formatter ast_mpi in
    flush outfile;
    close_out outfile;
    Printf.printf "Multi-ML Ast available in: %s\n" astout_multi_ml;
    ast_mpi
  with
  | (Lexer.Error(Lexer.Unterminated_comment _, _) )->
     print_string "Unterminated_comment\n"
    ;exit 1
  | Syntaxerr.Error e ->
     print_string "Syntax Error\n";
    Syntaxerr.report_error Format.std_formatter e
    ;exit 1
  (*TODO : position, ligne, etc.*)
  | Parsing.Parse_error->
     ignore (print_string "Parsing Error\n");
    let loc = Location.curr lexbuf in
    ignore (raise(Syntaxerr.Error(Syntaxerr.Other loc)))
    ;exit 1
  (*Handle typing errors*)
  | Typing_errors.Unbound_Value (s,l) -> 
     Typing_errors.handle_unbounded_values s l; exit 1
  | Typing_errors.Wrong_Type (g,e,l) -> 
     Typing_errors.handle_wrong_type g e l; exit 1
  | Typing_errors.Wrong_Locality (g,e,l) -> 
     Typing_errors.handle_wrong_locality g e l; exit 1
  | Typing_errors.Vector_Imbrication l -> 
     Typing_errors.handle_vector_imbrication l; exit 1
  | Typing_errors.Non_Communicable (e,l) -> 
     Typing_errors.handle_non_communicable e l; exit 1
;;

(*Génération du code intermédiaire histoire de pouvoir debugger*)

let multi_ml_mpi_code = "./multi_ml_mpi_code.ml";;
                          
(* let printer = Pprintast_seq.string_of_structure ast_multi_ml in *)

  let printer = "
                 open Headers;;
                 print_string \"yolo\"

" in
    let outfile = open_out multi_ml_mpi_code in
    let _ = output_string outfile printer in
    flush outfile;
    close_out outfile;

