#include <mpi.h>
#include <stdio.h>
#include <string.h>
#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h> 
#include <fcntl.h>

#include "comm_mpi.h"

typedef enum {ROOT, NODE, LEAF} level_type;

MPI_Comm MPI_COMM_PARENT,
  MPI_COMM_CHILDREN,
  MPI_COMM_SIBLINGS;

MPI_Group MPI_GROUP_PARENT,
  MPI_GROUP_CHILDREN,
  MPI_GROUP_SIBLINGS,
  MPI_GROUP_PARENT_U_CHILDREN;

int pid,sid;
int nb_siblings,nb_children;

int memory_type;

char hostname[512];

int __DEBUG__ = 0;

value ocaml_mpi_hello(){

  int rank, size;
 
  MPI_Init (NULL,NULL);      /* starts MPI */
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);        /* get current process id */
  MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */
  printf( "Hello world from process %i of %i\n", rank, size );
  MPI_Finalize();

}

value ocaml_mpi_init(value arguments)
{
  int argc, i;
  char ** argv;

  CAMLparam1 (arguments);
  argc = Wosize_val(arguments);
  argv = (char**)stat_alloc((argc + 1) * sizeof(char *));
  for (i = 0; i < argc; i++) argv[i] = String_val(Field(arguments, i));
  argv[i] = NULL;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  gethostname(hostname, 512);
  //MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  CAMLreturn(Val_unit);
}


value ocaml_mpi_finalize(){
  MPI_Finalize();
}

value ocaml_mpi_pid(value unit){
  CAMLparam1(unit);
  /* MPI_Comm_rank (MPI_COMM_WORLD, &pid); */
  CAMLreturn(Val_int(pid));
}

value ocaml_mpi_sid(value unit){
  CAMLparam1(unit);
  //MPI_Comm_rank (MPI_COMM_SIBLINGS, &pid);
  CAMLreturn(Val_int(sid));
}

/* value ocaml_mpi_nprocs(value unit){ */
/*   CAMLparam1(unit); */
/*   int nprocs;  */
/*   MPI_Comm_size (MPI_COMM_WORLD, &nprocs); */
/*   CAMLreturn(Val_int(nprocs)); */
/* } */

value ocaml_mpi_nbsiblings(value unit){
  CAMLparam1(unit);
  /* int nprocs;  */
  /* MPI_Comm_size (MPI_COMM_SIBLINGS, &nprocs); */
  CAMLreturn(Val_int(nb_siblings));
}

value ocaml_mpi_nbchildren(value unit){
  CAMLparam1(unit);
  CAMLreturn(Val_int(nb_children));
}

value ocaml_mpi_hostname(value unit){
  CAMLparam1(unit);
  int len = strlen(hostname);
  value c = caml_alloc_string(len);
  memcpy(String_val(c),hostname,len);
  CAMLreturn(c);
}

value ocaml_mpi_wtime(value unit)
{
  CAMLparam1(unit);
  CAMLreturn(copy_double(MPI_Wtime()));
}

value ocaml_mpi_init_archi(value l, value f, value s, value b, value m){
  
  CAMLparam4(l,f,s,b);

  int level = Int_val(l);
  int parent = Int_val(f);
  int children = Int_val(s);
  int siblings = Int_val(b);
  memory_type = Int_val(m);

  MPI_Comm tmp_c;
  MPI_Comm_split(MPI_COMM_WORLD, siblings, pid, &tmp_c);

  char name[64];
  char c[2];
  sprintf(c, "%d", siblings);
  strcpy(name,"MPI_Comm_B");
  strcat(name,c);
  MPI_COMM_SIBLINGS = tmp_c;
  MPI_Comm_set_name(MPI_COMM_SIBLINGS,name);

  if(level % 2 != 0){
    //printf("Pid %i va créér ses pères\n",pid);
    MPI_Comm_split(MPI_COMM_WORLD,parent,pid,&tmp_c);
    MPI_COMM_PARENT = tmp_c;
    sprintf(c, "%d", parent);
    strcpy(name,"MPI_Comm_F");
    strcat(name,c);
    MPI_Comm_set_name(MPI_COMM_PARENT,name);

    
  }
  else{
    //printf("Pid %i va créér ses fils\n",pid);
    MPI_Comm_split(MPI_COMM_WORLD,children,pid,&tmp_c);
    MPI_COMM_CHILDREN = tmp_c;
    sprintf(c, "%d", children);
    strcpy(name,"MPI_Comm_S");
    strcat(name,c);
    MPI_Comm_set_name(MPI_COMM_CHILDREN,name);

  }

  
  if(level % 2 != 0){
    //printf("Pid %i va créér ses fils\n",pid);
    MPI_Comm_split(MPI_COMM_WORLD,children,pid,&tmp_c);
    MPI_COMM_CHILDREN = tmp_c;
    sprintf(c, "%d", children);
    strcpy(name,"MPI_Comm_S");
    strcat(name,c);
    MPI_Comm_set_name(MPI_COMM_CHILDREN,name);

  }
  else{
    //printf("Pid %i va créér ses pères\n",pid);
    MPI_Comm_split(MPI_COMM_WORLD,parent,pid,&tmp_c);
    MPI_COMM_PARENT = tmp_c;
    sprintf(c, "%d", parent);
    strcpy(name,"MPI_Comm_F");
    strcat(name,c);
    MPI_Comm_set_name(MPI_COMM_PARENT,name);

  }

  //Ceux qui appartiennent au groupe bidon
  if (parent == 0){ 
    MPI_COMM_PARENT = MPI_COMM_SELF;
    MPI_Comm_set_name(MPI_COMM_PARENT,"MPI_COMM_SELF");
  }
  if (children == 0){
    MPI_COMM_CHILDREN = MPI_COMM_SELF;
    MPI_Comm_set_name(MPI_COMM_CHILDREN,"MPI_COMM_SELF");
  }

  //Set de la taille de comms
  MPI_Comm_size (MPI_COMM_CHILDREN, &nb_children);
  //On retire sois même
  nb_children--;
  MPI_Comm_size (MPI_COMM_SIBLINGS, &nb_siblings);

  //Set groups
  MPI_Comm_group(MPI_COMM_PARENT,&MPI_GROUP_PARENT);
  MPI_Comm_group(MPI_COMM_CHILDREN,&MPI_GROUP_CHILDREN);
  MPI_Comm_group(MPI_COMM_SIBLINGS,&MPI_GROUP_SIBLINGS);
  MPI_Group_union(MPI_GROUP_PARENT,MPI_GROUP_CHILDREN,&MPI_GROUP_PARENT_U_CHILDREN);

  //Set sid
  MPI_Comm_rank (MPI_COMM_SIBLINGS, &sid);
  
  //Affichage debug
  if(1){
  char name_siblings[MPI_MAX_OBJECT_NAME], 
    name_children[MPI_MAX_OBJECT_NAME],
    name_parent[MPI_MAX_OBJECT_NAME];

  name_parent[0] = 0;
  name_children[0] = 0;
  name_siblings[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen );
  MPI_Comm_get_name(MPI_COMM_PARENT, name_parent, &rlen );
  MPI_Comm_get_name(MPI_COMM_SIBLINGS, name_siblings, &rlen );
  printf("Pid : %i, Parent : %s, Children[%i] : %s, Siblings[%i] : %s \n",pid,name_parent,nb_children,name_children,nb_siblings,name_siblings);

  int sf,ss,sb,sg;
  MPI_Comm_size(MPI_COMM_PARENT,&sf);
  MPI_Comm_size(MPI_COMM_CHILDREN,&ss);
  MPI_Comm_size(MPI_COMM_SIBLINGS,&sb);
  MPI_Group_size(MPI_GROUP_PARENT_U_CHILDREN, &sg);
  printf("Pid : %i (%s), Parent[%i], Children[%i], Siblings[%i], SUF[%i] \n",pid,hostname,sf,ss,sb,sg);
  }
  
  MPI_Barrier(MPI_COMM_WORLD);

  CAMLreturn(Val_unit);
}

/* /\*C'est manuel pour l'instant !*\/ */
/* value ocaml_mpi_init_archi( value c1, value c2, value c3){ */
/*   CAMLparam3(c1,c2,c3); */
  
/*   int color1 = Int_val(c1); */
/*   int color2 = Int_val(c2); */
/*   int color3 = Int_val(c3); */
/*   int size, grp_pid,children_color; */
  

/*   MPI_COMM_CHILDREN = MPI_COMM_SELF; */
/*   MPI_COMM_PARENT = MPI_COMM_SELF; */

  
/*   MPI_Comm tmp_c; */
/*   MPI_Comm_split(MPI_COMM_WORLD, color1, pid, &tmp_c); */
/*   if(pid == 0 || pid == 1 || pid == 2){ */
/*     if (pid == 0){ */
/*       MPI_COMM_CHILDREN = tmp_c; */
/*       MPI_Comm_set_name(MPI_COMM_CHILDREN,"MPI_Comm_F0"); */
/*       MPI_COMM_PARENT = MPI_COMM_SELF; */
/*     } */
/*     else { */
/*       MPI_COMM_PARENT = tmp_c; */
/*       MPI_Comm_set_name(MPI_COMM_PARENT,"MPI_Comm_F0"); */
/*     } */
/*   } */
/*   else{ */
/*     MPI_Comm_set_name(MPI_COMM_CHILDREN,"MPI_MPI_COMM_CHILDREN_SELF"); */
/*   } */

/*   MPI_Comm_split(MPI_COMM_WORLD,color2,pid,&tmp_c); */
/*   if(pid ==0) {} */
/*   else{ */
/*     if(pid == 1){ */
/*       MPI_COMM_CHILDREN = tmp_c; */
/*       MPI_Comm_set_name(MPI_COMM_CHILDREN,"MPI_Comm_F1"); */
/*     } else */
/*     if(pid == 2){ */
/*       MPI_COMM_CHILDREN = tmp_c; */
/*       MPI_Comm_set_name(MPI_COMM_CHILDREN,"MPI_Comm_F2"); */
/*     } */
/*     else */
/*       if(pid == 3 || pid == 4){ */
/*   	MPI_COMM_PARENT = tmp_c; */
/*   	MPI_Comm_set_name(MPI_COMM_PARENT,"MPI_Comm_F1"); */
/*       } */
/*     else */
/*       if(pid == 5 || pid == 6){ */
/*   	MPI_COMM_PARENT = tmp_c; */
/*   	MPI_Comm_set_name(MPI_COMM_PARENT,"MPI_Comm_F2"); */
/*       } */
/*     } */
  
/*   //  Création des groupe de siblings */
/*   MPI_Comm_split(MPI_COMM_WORLD,color3,pid,&tmp_c); */
/*   char b_name[64]; */
/*   char c[2]; */
/*   sprintf(c, "%d", color3); */
/*   strcpy(b_name,"MPI_Comm_B"); */
/*   strcat(b_name,c); */
/*   MPI_COMM_SIBLINGS = tmp_c; */
/*   MPI_Comm_set_name(MPI_COMM_SIBLINGS,b_name); */

/*   MPI_Comm_group(MPI_COMM_PARENT,&MPI_GROUP_PARENT); */
/*   MPI_Comm_group(MPI_COMM_CHILDREN,&MPI_GROUP_CHILDREN); */
/*   MPI_Comm_group(MPI_COMM_SIBLINGS,&MPI_GROUP_SIBLINGS); */

/*   if(1){ */
/*   char name_siblings[MPI_MAX_OBJECT_NAME],  */
/*     name_children[MPI_MAX_OBJECT_NAME], */
/*     name_parent[MPI_MAX_OBJECT_NAME]; */

/*   name_parent[0] = 0; */
/*   name_children[0] = 0; */
/*   name_siblings[0] = 0; */
/*   int rlen; */
/*   MPI_Comm_size (MPI_COMM_CHILDREN, &nb_children); */
/*   MPI_Comm_size (MPI_COMM_SIBLINGS, &nb_siblings); */
/*   MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen ); */
/*   MPI_Comm_get_name(MPI_COMM_PARENT, name_parent, &rlen ); */
/*   MPI_Comm_get_name(MPI_COMM_SIBLINGS, name_siblings, &rlen ); */
/*   //printf("Pid : %i, Parent : %s, Children[%i] : %s, Siblings[%i] : %s \n",pid,name_parent,nb_children,name_children,nb_siblings,name_siblings); */

/*   } */
  
/*   MPI_Barrier(MPI_COMM_WORLD); */
 
/*   CAMLreturn(Val_unit); */
/* } */


/* value ocaml_mpi_inti_archi(value va, value c){ */
/*   CAMLparam2(va,c); */

/*   int val = Int_val(va); */
/*   int color = Int_val(c); */
  
/*   int size, grp_pid,children_color; */

/*   MPI_Comm_split(MPI_COMM_WORLD, color, pid, &my_comm_group); */
/*   MPI_Comm_group(my_comm_group,&my_group); */

/*   MPI_Group_size(my_group,&size); */
/*   MPI_Group_rank(my_group,&grp_pid); */

/*   /\*Il faudra automatiser ça ;)*\/ */
/*   if(pid == 0){ */
/*     children_color = 1; */
/*   }else */
/*     if(pid == 1){ */
/*       children_color = 2; */
/*     }else */
/*       if(pid == 2){ */
/* 	children_color = 3; */
/*       }else */
/* 	children_color = 4; */

/*   char level[5]; */
/*   if(pid == 0){ */
/*     strcpy(level,"Root"); */
/*     my_type = ROOT; */
/*   } */
/*   else */
/*     if(children_color == -1){ */
/*       strcpy(level,"Leaf"); */
/*       my_type = LEAF; */
/*     } */
/*     else{ */
/*       strcpy(level,"Node"); */
/*       my_type = NODE; */
/*     } */

/*   int v = 0; */
/*   MPI_Comm_split(MPI_COMM_WORLD, children_color, v, &my_MPI_COMM_CHILDREN); */
/*   MPI_Comm_group(my_MPI_COMM_CHILDREN,&my_children); */

/*   //sleep(p); */
/*   //printf("%s, pid wolrd %i, pid group %i (group size %i), color = %i, children color = %i\n",level,pid,grp_pid,size,color,children_color); */
/* } */

value ocaml_mpi_test(value p, value c){
  CAMLparam2(p,c);
  MPI_Status status;

  char name[MPI_MAX_OBJECT_NAME], 
    name_children[MPI_MAX_OBJECT_NAME],
    name_parent[MPI_MAX_OBJECT_NAME];

  name_parent[0] = 0;
  name_children[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen );


  int grpid;
  MPI_Comm_rank(MPI_COMM_CHILDREN, &grpid);
  int np;
  MPI_Comm_size(MPI_COMM_CHILDREN, &np);
  /*
  sleep(pid);
  //printf("S pid : %i -> %i/%i\n",pid,grpid,np);

  MPI_Comm_rank(MPI_COMM_PARENT, &grpid);
  MPI_Comm_size(MPI_COMM_PARENT, &np);

  //printf("F pid : %i -> %i/%i\n",pid,grpid,np);
  */
  
  int recv[3];
  if(pid == 1 || pid == 2){
    int rank;
    MPI_Comm_rank(MPI_COMM_PARENT,&rank);
    int buff = pid;
    //MPI_Recv(&buf,1,MPI_INT,0,0,MPI_COMM_PARENT,&status);
    MPI_Gather(&buff,1,MPI_INT,recv,1,MPI_INT,0,MPI_COMM_PARENT);
  }
  else 
    if (pid == 0){
      int rank;
      MPI_Comm_rank(MPI_COMM_CHILDREN,&rank);
      int buff = 8;
      //MPI_Send(&buf,1,MPI_INT,1,0,MPI_COMM_CHILDREN);
      MPI_Gather(&buff,1,MPI_INT,&recv[0],1,MPI_INT,rank,MPI_COMM_CHILDREN);
      //printf("j'ai recu %i, %i, %i\n",recv[0],recv[1],recv[2]);
    }
  
  MPI_Barrier(MPI_COMM_WORLD);
  CAMLreturn(Val_unit);

}
/*
//Envoie une fonction marshalisée sous la forme de bytes
value ocaml_mpi_send_mfun_b(value str,value p){
  CAMLparam2(str,p);

  int len = caml_string_length(str);
  char *to_send = String_val(str);
  int dest_pid = Int_val(p);
  
  MPI_Send(&len,1,MPI_INT,dest_pid,0,MPI_COMM_PARENT);
  MPI_Send(to_send,len,MPI_CHAR,dest_pid,0,MPI_COMM_PARENT);
  CAMLreturn(Val_unit);
  }*/

//Recoit une fonction marshalisée sous la forme de bytes et retour la string ocaml bien formée
/*value ocaml_mpi_recv_mfun_b(value p){

  CAMLparam1(p);

  MPI_Status status;
  int dest_pid = Int_val(p);

  int len;
  MPI_Recv(&len,1,MPI_INT,dest_pid,0,MPI_COMM_CHILDREN,&status);
  char *out = malloc(sizeof(char)*len);
  MPI_Recv(out,len,MPI_CHAR,dest_pid,0,MPI_COMM_CHILDREN,&status);

  value c = caml_alloc_string(len);
  //hé ouais ! Sinon le '\0' casse tout !
  memcpy(String_val(c),out,len);

  CAMLreturn(c);
  }*/
/*
value ocaml_mpi_recv_mfun_at_children(value p){

  CAMLparam1(p);

  MPI_Status status;
  int dest_pid = Int_val(p);

  int len;
  MPI_Recv(&len,1,MPI_INT,dest_pid,0,MPI_COMM_CHILDREN,&status);
  char *out = malloc(sizeof(char)*len);
  MPI_Recv(out,len,MPI_CHAR,dest_pid,0,MPI_COMM_CHILDREN,&status);

  value c = caml_alloc_string(len);
  //hé ouais ! Sinon le '\0' casse tout !
  memcpy(String_val(c),out,len);

  CAMLreturn(c);
  }*/

//Envoie une fonction marshalisée sous la forme de bytes
/*value ocaml_mpi_send_mfun(value str,value p){
  CAMLparam2(str,p);

  int len = caml_string_length(str);
  char *to_send = String_val(str);
  int dest_pid = Int_val(p);
  
  MPI_Send(&len,1,MPI_INT,dest_pid,0,MPI_COMM_WORLD);
  MPI_Send(to_send,len,MPI_CHAR,dest_pid,0,MPI_COMM_WORLD);
  
  CAMLreturn(Val_unit);
  }*/

//Recoit une fonction marshalisée sous la forme de bytes et retour la string ocaml bien formée
/*value ocaml_mpi_recv_mfun(value p){

  CAMLparam1(p);

  MPI_Status status;
  int dest_pid = Int_val(p);

  int len;
  MPI_Recv(&len,1,MPI_INT,dest_pid,0,MPI_COMM_WORLD,&status);
  char *out = malloc(sizeof(char)*len);
  MPI_Recv(out,len,MPI_CHAR,dest_pid,0,MPI_COMM_WORLD,&status);

  value c = caml_alloc_string(len);
  //hé ouais ! Sinon le '\0' casse tout !
  memcpy(String_val(c),out,len);

  CAMLreturn(c);
  }*/

/* value ocaml_mpi_bcast_mfun_children(value str){ */

/*   CAMLparam1(str); */

/*   //printf("Bcast send (on children) by pid %i\n",pid); */

/*   int len = caml_string_length(str); */
/*   char *to_send = String_val(str); */

/*   MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_CHILDREN); */
/*   MPI_Bcast(to_send,len,MPI_CHAR,0,MPI_COMM_CHILDREN); */

/*   CAMLreturn(Val_unit); */
/*   /\*if (__DEBUG__) { */
/*     //printf("Bcast Send len %i, val :\n",len); */
/*     int i = 0; */
/*     for(;i<len;i++) */
/*       //printf("%c",to_send[i]); */
/*     //printf("\n"); */
/*     }*\/ */
/* } */

/* value ocaml_mpi_recv_mfun_parent(value unit){ */

/*   //printf("Bcast recv (on parent) by pid %i\n",pid); */

/*   CAMLparam1(unit); */


/*   int len; */
/*   MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_PARENT); */

/*   char *out = malloc(sizeof(char)*len); */
/*   MPI_Bcast(out,len,MPI_CHAR,0,MPI_COMM_PARENT); */


/*   value c = caml_alloc_string(len); */
/*   //hé ouais ! Sinon le '\0' casse tout ! */
/*   memcpy(String_val(c),out,len); */


/*   /\*if (__DEBUG__) { */
/*     //printf("Bcast Recv len %i, val :\n",len); */
/*     int i = 0; */
/*     for(;i<len;i++) */
/*       //printf("%c",out[i]); */
/*     //printf("\n"); */
/*     }*\/ */
  
/*   CAMLreturn(c); */
/* } */

/* //Retourne un chaîne contenant les n valeurs des fils concaténées */
/* value ocaml_mpi_gather_parent(value unit){ */

/*   CAMLparam1(unit); */
  
/*   /\* //printf("Je suis le père et je vais gather\n"); *\/ */

/*   int TAILLE = 3; */

/*   int rank = 0; */
/*   int *counts = malloc(sizeof(int)*TAILLE); */
/*   int disps[TAILLE]; */
/*   int b = 0; */

  
/*   MPI_Gather(&b,1,MPI_INT,counts,1,MPI_INT,0,MPI_COMM_CHILDREN); */

/*   int i = 0; */
/*   int res = 0; */
/*   for(; i < 3; i++){ */
/*     disps[i] = res; */
/*     res += counts[i]; */
/*     /\* //printf("disps[%i] = %i\n",i,disps[i]); *\/ */
/*     /\* //printf("Counts[%i] = %i\n",i,counts[i]); *\/ */
/*   } */
/*   char recv[res]; */


/*   MPI_Gatherv(&b, 0, MPI_CHAR, recv, counts, disps, MPI_CHAR, 0, MPI_COMM_CHILDREN); */

/*   /\* i = 0; *\/ */
/*   /\* for(; i < res; i++){ *\/ */
/*   /\*   //printf("recv[%i]=%i\n",i,recv[i]); *\/ */
/*   /\*   } *\/ */

/*   /\* i = 0; *\/ */
/*   /\* //printf("C (%i): ",counts[1]); *\/ */
/*   /\* for(; i<counts[1];i++) //printf("%c",recv[i+counts[0]]); *\/ */
/*   /\* //printf("\n"); *\/ */

/*   int len = counts[1]+counts[2]; */
/*   value c = caml_alloc_string(len); */
/*   char * das_out = recv; */
/*   //hé ouais ! Sinon le '\0' casse tout ! */
/*   memcpy(String_val(c),das_out,len); */

/*   CAMLreturn(c); */

/* } */

/* value ocaml_mpi_gather_children(value str){ */
/*   CAMLparam1(str); */

/*   int len = caml_string_length(str); */
/*   char *to_send = String_val(str); */
  
/*   /\* //printf("Je suis le fils et je vais gather\n"); *\/ */
  

/*   char recv; */
/*   int count = len; */
/*   int *disps; */

/*   /\* //printf("len C: %i \nval C :[",len); *\/ */
/*   /\* int i = 0; *\/ */
/*   /\* for(; i<len;i++) //printf("%c",to_send[i]); *\/ */
/*   /\* //printf("]\n"); *\/ */

/*   MPI_Gather(&count,1,MPI_INT,&recv,1,MPI_INT,0,MPI_COMM_PARENT); */
/*   MPI_Gatherv(to_send, count, MPI_CHAR, &recv, &count, disps, MPI_CHAR, 0, MPI_COMM_PARENT); */

/*   CAMLreturn(Val_unit); */
/* } */

value ocaml_mpi_alltoall_int(value data, value result)
{
  CAMLparam2(data,result);
  MPI_Alltoall(&Field(data, 0), 1, MPI_LONG,
	       &Field(result, 0), 1, MPI_LONG,
	       MPI_COMM_SIBLINGS);
  CAMLreturn(Val_unit);
} 

static void ocaml_mpi_counts_displs(value lengths,
                                   /* out */ int ** counts,
                                   /* out */ int ** displs)
{
  int size, disp, i;

  size = Wosize_val(lengths);
  if (size > 0) {
    *counts = stat_alloc(size * sizeof(int));
    *displs = stat_alloc(size * sizeof(int));
    for (i = 0, disp = 0; i < size; i++) {
      (*counts)[i] = Int_val(Field(lengths, i));
      (*displs)[i] = disp;
      disp += (*counts)[i];
    }
  } else {
    *counts = NULL;
    *displs = NULL;
  }
}

value ocaml_mpi_alltoall(value sendbuf, value sendlengths,
			value recvbuf, value recvlengths)
{
  int * recvcounts, * recvdispls;
  int * sendcounts, * senddispls;
  
  CAMLparam4(sendbuf,sendlengths,recvbuf,recvlengths);
  ocaml_mpi_counts_displs(sendlengths, &sendcounts, &senddispls);
  ocaml_mpi_counts_displs(recvlengths, &recvcounts, &recvdispls);
  MPI_Alltoallv(String_val(sendbuf), sendcounts, senddispls, MPI_BYTE,
		String_val(recvbuf), recvcounts, recvdispls, MPI_BYTE,
		MPI_COMM_SIBLINGS);
  if (recvcounts != NULL) {
    stat_free(recvcounts);
    stat_free(recvdispls);
  }
  if (sendcounts != NULL) {
    stat_free(sendcounts);
    stat_free(senddispls);
  }
  CAMLreturn(Val_unit);
}


value c_usleep(value s){
  CAMLparam1(s);
  usleep(s);
  CAMLreturn(Val_unit);
}

/*************/
/*  Dev fun  */
/*************/

value ocaml_mpi_distribute_job_to_children(value str){

  CAMLparam1(str);

  int len = caml_string_length(str);
  char *to_send = String_val(str);

  MPI_Request request;
  MPI_Status status;

  int i = 1;

  //Il va falloir optimiser toutes ces communications mais ce n'est pas le problème pour l'instant

  //Envoyer les fonctions aux fils

  char name_children[MPI_MAX_OBJECT_NAME];
   name_children[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen );
  
  //Essayer avec un broadcast !!
  for(;i<=nb_children;i++){
    //printf("I am %i. On %s. I send to %i",pid,name_children ,i);
    int dest_pid = i;
    MPI_Send(&len,1,MPI_INT,dest_pid,0,MPI_COMM_CHILDREN);
    //printf("…");
    MPI_Send(to_send,len,MPI_CHAR,dest_pid,0,MPI_COMM_CHILDREN);
    //printf("ok\n");
    }
  //MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_CHILDREN);
  //MPI_Bcast(to_send,len,MPI_CHAR,0,MPI_COMM_CHILDREN);

  CAMLreturn(Val_unit);
  }

value ocaml_mpi_wait_job(value unit){
  
  CAMLparam1(unit);

  MPI_Status status;
  int src_pid = 0;

  char name_parent[MPI_MAX_OBJECT_NAME];
  name_parent[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_PARENT, name_parent, &rlen );

  //printf("I am %i. I wait data on %s …\n",pid,name_parent);

  int len;
  MPI_Recv(&len,1,MPI_INT,src_pid,0,MPI_COMM_PARENT,&status);
  //MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_PARENT);

  //printf("…");

  char *out;
  value c = caml_alloc_string(len);

  if (len == -1) {
    out = malloc(sizeof(char)*2);
    out = "-1";
  }
  else{
    out = malloc(sizeof(char)*len);
    MPI_Recv(out,len,MPI_CHAR,src_pid,0,MPI_COMM_PARENT,&status);
    //MPI_Bcast(out,len,MPI_CHAR,0,MPI_COMM_PARENT);
  }

  //printf("ok\n");

  //hé ouais ! Sinon le '\0' casse tout !
  memcpy(String_val(c),out,len);

  CAMLreturn(c);
  }

//Warning faire la modif en cas de mémoire distribuée
value ocaml_mpi_distribute_job_to_children_unsafe_mmap(value str){

  CAMLparam1(str);

  int len = caml_string_length(str);
  char *to_send = String_val(str);

  MPI_Request request;
  MPI_Status status;

  int i = 1;

  //Il va falloir optimiser toutes ces communications mais ce n'est pas le problème pour l'instant

  //Envoyer les fonctions aux fils

  char name_children[MPI_MAX_OBJECT_NAME];
   name_children[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen );

  MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_CHILDREN);

  char *msg = to_send;
  size_t shmsize = len;
  unsigned *shmaddr;

  char name[1024];
  char s[15];
  sprintf(s, "%d", len);
  strcpy(name,"/test");
  strcat(name,s);
  int shmfd = shm_open(name,O_CREAT | O_RDWR, 0600);
  int ftr = ftruncate(shmfd,shmsize);
  shmaddr = (unsigned *)mmap(0, shmsize, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);

  memcpy(shmaddr, msg, len);

  /*MPI_Barrier(MPI_COMM_CHILDREN);

  long ptr_zmem = (long)shmaddr;
  printf("send : %p\n",(int*)ptr_zmem);
  MPI_Bcast(&ptr_zmem,1,MPI_LONG,0,MPI_COMM_CHILDREN);*/

  MPI_Barrier(MPI_COMM_CHILDREN);

  //shm_unlink(name);

  CAMLreturn(Val_unit);
}

//Warning faire la modif en cas de mémoire distribuée
value ocaml_mpi_wait_job_unsafe_mmap(value unit){
  
  CAMLparam1(unit);

  MPI_Status status;
  int src_pid = 0;

  char name_parent[MPI_MAX_OBJECT_NAME];
  name_parent[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_PARENT, name_parent, &rlen );

  //printf("I am %i. I wait data on %s …\n",pid,name_parent);

  int len;
  MPI_Bcast(&len,1,MPI_INT,0,MPI_COMM_PARENT);

  char *out;
  value c = caml_alloc_string(len);

  if (len == -1) {
    out = malloc(sizeof(char)*2);
    out = "-1";
  }
  else{
    out = malloc(sizeof(char)*len);
  }

  /*MPI_Barrier(MPI_COMM_PARENT);

  long ptr_zmem;
  MPI_Bcast(&ptr_zmem,1,MPI_LONG,0,MPI_COMM_PARENT);
  printf("recv : %p\n",(int*)ptr_zmem);*/

  MPI_Barrier(MPI_COMM_PARENT);
  
  int recv_len = len;
  size_t shmsize = recv_len;
  unsigned *shmaddr;
  
  char name[1024];
  char str[15];
  sprintf(str, "%d", len);
  strcpy(name,"/test");
  strcat(name,str);
  int shmfd = shm_open(name,O_CREAT | O_RDWR, 0600);
  shmaddr = (unsigned *)mmap(0, shmsize, PROT_READ | PROT_WRITE, MAP_SHARED , shmfd, 0);
  
  //shmaddr = (int*)ptr_zmem;
  //printf("shma : %p\n",shmaddr);

  char *buff = malloc(sizeof(char)*(recv_len));
  memcpy(buff,shmaddr,recv_len);

  out = buff;

  out[len] = 0;
  buff[recv_len] = 0;

  //hé ouais ! Sinon le '\0' casse tout !
  memcpy(String_val(c),out,len);


  CAMLreturn(c);
}

value ocaml_mpi_distribute_id_to_children(value id){

  CAMLparam1(id);

  int id_to_send = Int_val(id);

  MPI_Request request;
  MPI_Status status;

  int i = 1;

  //Il va falloir optimiser toutes ces communications mais ce n'est pas le problème pour l'instant

  //Envoyer les fonctions aux fils

  char name_children[MPI_MAX_OBJECT_NAME];
  name_children[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen );

  //MPI_Bcast(&id_to_send,1,MPI_INT,0,MPI_COMM_CHILDREN);

  for(;i<=nb_children;i++){
    //printf("I am %i. On %s. I send to %i",pid,name_children ,i);
    int dest_pid = i;
    MPI_Send(&id_to_send,1,MPI_INT,dest_pid,0,MPI_COMM_CHILDREN);
    //printf("ok\n");
  }
  CAMLreturn(Val_unit);
}

value ocaml_mpi_wait_id(value unit){

  CAMLparam1(unit);

  MPI_Status status;
  int src_pid = 0;

  int id;

  MPI_Recv(&id,1,MPI_INT,src_pid,0,MPI_COMM_PARENT,&status);

  //MPI_Bcast(&id,1,MPI_INT,0,MPI_COMM_PARENT);

  CAMLreturn(Val_int(id));

}


//Envoie une fonction marshalisée sous la forme de bytes
value ocaml_mpi_send_mfun_parent(value str){
  CAMLparam1(str);

  int len = caml_string_length(str);
  char *to_send = String_val(str);
  int dest_pid = 0;

  MPI_Request request;
  MPI_Status status;

  
  MPI_Send(&len,1,MPI_INT,dest_pid,0,MPI_COMM_PARENT);
  MPI_Send(to_send,len,MPI_CHAR,dest_pid,0,MPI_COMM_PARENT);

  CAMLreturn(Val_unit);
}

//Recoit une fonction marshalisée sous la forme de bytes et retour la string ocaml bien formée
value ocaml_mpi_recv_mfun_child(value unit){

  CAMLparam1(unit);

  MPI_Status status;
  int recv_pid = 1;

  int len;
  MPI_Recv(&len,1,MPI_INT,recv_pid,0,MPI_COMM_CHILDREN,&status);
  char *out = malloc(sizeof(char)*len);
  MPI_Recv(out,len,MPI_CHAR,recv_pid,0,MPI_COMM_CHILDREN,&status);

  value c = caml_alloc_string(len);
  //hé ouais ! Sinon le '\0' casse tout !
  memcpy(String_val(c),out,len);

  CAMLreturn(c);
}


/*Pour envoyer une valeur à un fils i*/
value ocaml_mpi_distribute_job_to_child_i(value str, value id){

  CAMLparam2(str,id);

  int len = caml_string_length(str);
  char *to_send = String_val(str);

  MPI_Request request;
  MPI_Status status;

  //printf("Len of string = %i\n",len);

  int dest_pid = Int_val(id);

  //Il va falloir optimiser toutes ces communications mais ce n'est pas le problème pour l'instant

  char name_children[MPI_MAX_OBJECT_NAME];
  name_children[0] = 0;
  int rlen;
  MPI_Comm_get_name(MPI_COMM_CHILDREN, name_children, &rlen );

  //printf("I am %i. On %s. I send to %i",pid,name_children ,dest_pid);
  MPI_Send(&len,1,MPI_INT,dest_pid,0,MPI_COMM_CHILDREN);
  //printf("…");
  MPI_Send(to_send,len,MPI_CHAR,dest_pid,0,MPI_COMM_CHILDREN);
  //printf("ok\n");
 
  CAMLreturn(Val_unit);
}


value ocaml_mpi_gather_int_from_children(value recvlengths){
  CAMLparam1(recvlengths);

  int send_buf = Val_int(0);

  MPI_Gather(&send_buf,1,MPI_LONG,&Field(recvlengths,0),1,MPI_LONG,0,MPI_COMM_CHILDREN);

  CAMLreturn(Val_unit);
  
}

value ocaml_mpi_gather_int_to_parent(value i){
  CAMLparam1(i);

  int *recv_buf;
  
  MPI_Gather(&i,1,MPI_LONG,recv_buf,1,MPI_LONG,0,MPI_COMM_PARENT);
    
  CAMLreturn(Val_unit);
  
}

value ocaml_mpi_gather_mfun_from_children(value resbytes, value recv_lengths){
  CAMLparam2(resbytes,recv_lengths);

  int *recv_counts, *recv_displ;
  ocaml_mpi_counts_displs(recv_lengths,&recv_counts,&recv_displ);

  char *send_buf = NULL;

  MPI_Gatherv(send_buf,0,MPI_BYTE,String_val(resbytes),recv_counts,recv_displ,MPI_BYTE,0,MPI_COMM_CHILDREN);
  
  CAMLreturn(Val_unit);
}

value ocaml_mpi_gather_mfun_to_parent(value sendbytes, value send_length){
  CAMLparam2(sendbytes,send_length);

  char *recv_buf = NULL;

  int *recv_counts;
  int *recv_displ;
  
  MPI_Gatherv(String_val(sendbytes),Int_val(send_length),MPI_BYTE,recv_buf,recv_counts,recv_displ,MPI_BYTE,0,MPI_COMM_PARENT);
  
  CAMLreturn(Val_unit);
}

value ocaml_mpi_mbsp_barrier(value unit){
  CAMLparam1(unit);
  double start,end;
  if(pid==0)
    start=MPI_Wtime();
  MPI_Barrier(MPI_COMM_WORLD);
  if(pid==0){
    end=MPI_Wtime();
    double t=(end-start);
    printf("Multi-BSP synchronisation in %f s\n",t);
  }
  CAMLreturn(Val_unit);
}


value ocaml_mpi_barrier_children(value unit){
  MPI_Barrier(MPI_COMM_CHILDREN);
}


value ocaml_mpi_barrier_parent(value unit){
  MPI_Barrier(MPI_COMM_PARENT);
}
