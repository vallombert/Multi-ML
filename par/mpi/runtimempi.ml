include Parallel.Make(Parameters_in_file)(Mpi)

include Mpi
open Mpi

external init_archi : int -> int -> int -> int -> int -> unit = "ocaml_mpi_init_archi";;

let architecture = Parameters_in_file.architecture
(*let print_architecture = Parameters_in_file.print_architecture (Parameters_in_file.architecture_to_array architecture)*)
(*let _ = if pid () = 0 then Parameters_in_file.print_architecture architecture *)

let color_s = Parameters_in_file.get_children_color (pid())
                                                
let color_f = Parameters_in_file.get_parent_color (pid())

let color_b = Parameters_in_file.get_siblings_color (pid())
                                                    
let color_level = Parameters_in_file.get_level_color (pid())

let color_memory = Parameters_in_file.get_memory_color (pid())

let _ = init_archi color_level color_f color_s color_b color_memory

(*Init GID*)
let _ = Parameters_in_file.init_gid (pid())

(* external init_archi : int -> int -> int -> unit = "ocaml_mpi_init_archi" *)
external mpi_wait_job : unit -> bytes = "ocaml_mpi_wait_job"
external mpi_wait_args : unit -> bytes = "ocaml_mpi_wait_job"
external mpi_wait_id : unit -> int = "ocaml_mpi_wait_id"

external mpi_mbsp_barrier : unit -> unit = "ocaml_mpi_mbsp_barrier"                            

(* let _ = init_archi color_1 color_2 color_b *)

(*Table of stored values*)
let __htbl_did : (int, int -> int)  Hashtbl.t ref = ref (Hashtbl.create 1000)

let add_to_dyn ht id v =
  (*Printf.printf "Did_add : pid %i -> id %i\n" (pid()) id;*)
  Hashtbl.replace ht id ((Obj.magic)v)

let did_get id =
  try
    (* Printf.printf "Did_get %i\n" id; *)
    Hashtbl.find !__htbl_did id
  with
  | Not_found ->
     Printf.printf "Error did_get (pid %i) : Not_found -> id = %i\n%!" (pid()) id;
     let v = Printf.sprintf "Error did_get (pid %i) : Not_found -> id = %i" (pid()) id in
     failwith v 

let did_clean : int array -> unit =
  fun ids ->
    for i = 0 to Array.length ids do
      Hashtbl.remove !__htbl_did (ids.(i))
    done


let current_vector_id = ref (-1)
let current_reccall_vector_id = ref (-1)
let update_rccvid _ =
  (*  Printf.printf "old value :%i\n" (!current_reccall_vector_id);
  current_reccall_vector_id := !current_vector_id;
  Printf.printf "New value :%i\n" (!current_reccall_vector_id)*)
  ()

let __tree_id = ref 0
                    
let generate_tree_id _ = 
  let v = !__tree_id in
  (* Printf.printf "Generate tree id : pid %i -> id %i\n" (mbsp_pid ()) v; *)
  incr __tree_id;v
                   
(*Table of tree id to tree values*)
let __htbl_tree : (int, int -> int) Hashtbl.t ref = ref (Hashtbl.create 1000)

let add_to_tree_tbl id v =
  (* Printf.printf "Add_to_tree_tbl : pid %i -> id %i\n" (pid()) id; *)
  Hashtbl.replace !__htbl_tree id (Obj.magic v)

let get_from_tree_tbl id =
  try 
    Hashtbl.find !__htbl_tree id
  with 
  | Not_found ->
     Printf.printf "Error get_from_tree_tbl : Pid %i, Not_found -> id = %i\n%!" (pid()) id;
     let v = Printf.sprintf "Error get_from_tree_tbl : Pid %i, Not_found -> id = %i\n%!" (pid()) id
     in failwith v


(*Table of tree id link*)
let __htbl_tree_link : (int,int) Hashtbl.t ref = ref (Hashtbl.create 1000)

let add_to_tree_link id v =
  (* Printf.printf "Add_to_tree_link : pid %i : (%i -> %i)\n" (pid()) id v; *)
  Hashtbl.replace !__htbl_tree_link id (Obj.magic v)

let get_from_tree_link id =
    try 
    Hashtbl.find !__htbl_tree_link id
  with 
  | _ ->
     Printf.printf "Error get_from_tree_link : Not_found -> id = %i\n%!" id;
     let v = Printf.sprintf "Error get_from_tree_link : Not_found -> id = %i\n%!" id in
     failwith v
                  
let rec apply_args mf mtab =
  let (tab : int array) = Marshal.from_bytes mtab 0 in
  if Array.length tab = 0 then(
    (*Printf.printf "0 arg\n";*)
    let (f: unit -> 'b -> 'c ) = Marshal.from_bytes mf 0 in
    f ()
  )
  else if Array.length tab = 1 then (
    (* Printf.printf "1 arg\n"; *)
    let (f: 'a -> 'b -> 'c) = Marshal.from_bytes mf 0 in
    let v0 = (Obj.magic)(did_get (tab.(0))) in
    f v0
  ) else if Array.length tab = 2 then (
    (* Printf.printf "2 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    f v0 v1
  ) else if Array.length tab = 3 then (
    (* Printf.printf "3 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e ) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    f v0 v1 v2
  ) else if Array.length tab = 4 then (
    (* Printf.printf "4 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e -> 'f) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    let v3 = ((Obj.magic))(did_get (tab.(3))) in
    f v0 v1 v2 v3
  ) else if Array.length tab = 5 then (
    (* Printf.printf "5 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g ) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    let v3 = ((Obj.magic))(did_get (tab.(3))) in
    let v4 = ((Obj.magic))(did_get (tab.(4))) in
    f v0 v1 v2 v3 v4
  ) else if Array.length tab = 6 then (
    (* Printf.printf "6 arg\n"; *)
    let (f: 'a -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g -> 'h) = Marshal.from_bytes mf 0 in
    let v0 = ((Obj.magic))(did_get (tab.(0))) in
    let v1 = ((Obj.magic))(did_get (tab.(1))) in
    let v2 = ((Obj.magic))(did_get (tab.(2))) in
    let v3 = ((Obj.magic))(did_get (tab.(3))) in
    let v4 = ((Obj.magic))(did_get (tab.(4))) in
    let v5 = ((Obj.magic))(did_get (tab.(5))) in
    f v0 v1 v2 v3 v4 v5
  ) else failwith("Max args is 6 … TODO generic apply_args \n")

external mpi_send_mfun_to_children : bytes-> unit = "ocaml_mpi_distribute_job_to_children"
external mpi_send_mfun_to_parent : bytes -> unit = "ocaml_mpi_send_mfun_parent"
external gather_bytes_to_parent : bytes -> int -> unit = "ocaml_mpi_gather_mfun_to_parent"
external gather_int_to_parent : int -> unit = "ocaml_mpi_gather_int_to_parent"

external sleep : int -> unit = "c_usleep"
let _ = sleep ((pid())*0)

let rec wait_for_job () =
  let mjob = mpi_wait_job () in
  begin
    match Bytes.to_string mjob with
    | "gc" -> Printf.printf "Run GC bitch !\n";wait_for_job()
    (*Simple serial kill*)
    | "sk" ->
      if node () then mpi_send_mfun_to_children (Bytes.of_string "sk");
      Printf.printf "\x1b[31mKill pid %i @%s\x1b[0m\n" (pid ()) (hostname ());
      Obj.magic (fun _ -> -1)

    (*Serial kill with broadcast : multi-function call*)
    | "skb" ->
       let recv = mpi_wait_job () in
       let res = 
         if node () then (
	   mpi_send_mfun_to_children (Bytes.of_string "skb");
           let v = Marshal.from_bytes recv 0 in
           let m_v = Marshal.to_bytes v [Marshal.Closures] in
           mpi_send_mfun_to_children m_v;
           v
         )
         else (
           Marshal.from_bytes recv 0
         )
       in Printf.printf "\x1b[31mKill(T) pid %i @%s\x1b[0m\n%!" (pid ()) (hostname ());        
          Obj.magic (fun _ -> res)

    (*Build the final tree*)
    | "b_t" ->
       let mtv = mpi_wait_job () in
       let (f : unit -> 'a) = (Marshal.from_bytes mtv 0) in
       let id_tree,id_val = f () in
       let id_sub_val = get_from_tree_link id_val in
       if node () then(
         mpi_send_mfun_to_children (Bytes.of_string "b_t");
         (*Or mpi_send_id_to_children * 2 ? Fastest way ??*)
         let m_v = Marshal.to_bytes (fun _ -> (id_tree,id_sub_val)) [Marshal.Closures] in
         mpi_send_mfun_to_children m_v
       );
       Printf.printf "\x1b[31mKill(BT) pid %i @%s\x1b[0m\n%!" (pid ()) (hostname ());
       Obj.magic (fun _ -> id_tree)
 

    (*Working proj, but not really optimised*)
    (*| "proj" ->
       let id = mpi_wait_id () in
       (* let id = generate_dynamic_id () in *)
       let vl = (Obj.magic)(did_get id) in
       (* Printf.printf "Request Proj sur l'id no %i\n" id; *)
       let data = Array.init (nb_brothers()) (fun i -> vl) in
       let send_res = send data in
       let proj_res =
	 fun i ->
	 if not((0<=i) && (i<nb_children()))
	 then raise (Invalid_processor i)
	 else
	   send_res.(i) in
       let m_proj = Marshal.to_bytes proj_res [Marshal.Closures] in
       if bid () = 0 then (
	 mpi_send_mfun_to_parent m_proj);
       wait_for_job()*)

    (*Opt Proj with gather : not working*)
    | "proj" ->
       let id = mpi_wait_id () in
       let vl = (Obj.magic)(did_get id) in
       let to_send = Marshal.to_bytes vl [Marshal.Closures] in
       let len = Marshal.total_size to_send 0 in
       gather_int_to_parent len;
       gather_bytes_to_parent to_send len;
       parent_barrier();
       wait_for_job()
     
                   
    | "put" ->
       (*Todo*)
       let id = mpi_wait_id () in
      (* let id = generate_dynamic_id () in *)
      let (vf : int -> 'a) = (Obj.magic)(did_get id) in
      let data = Array.init (nb_siblings()) (fun i -> vf i) in
      let res = send data in
      let new_id = mpi_wait_id () in
      add_to_dyn !__htbl_did (new_id) (fun i -> res.(i));
      wait_for_job()

    | _ ->
       let id = mpi_wait_id () in
       current_vector_id := id;
       (* Printf.printf "Pid %i Build from vector no: %i\n" (pid()) (id); *)
       let margs = mpi_wait_args () in
       (*Eval de ce qui est reçu*)
       let eval = apply_args mjob margs in
       (*Ajout du binding*)
       add_to_dyn !__htbl_did (id) (eval);
       (*On retourne en attente*)
       wait_for_job ()
  end

let daemon () = wait_for_job ()


let __htbl_sid : (int, int -> int)  Hashtbl.t ref = ref (Hashtbl.create 1000)

let add_to_stat id v =
  Printf.printf "Sid_add : pid %i -> id %i\n" (pid()) id;
  Hashtbl.replace !__htbl_sid id ((Obj.magic)v)

let sid_get id =
  try
    (* Printf.printf "Did_get %i\n" id; *)
    Hashtbl.find !__htbl_sid id
  with
  | Not_found ->
    let v = Printf.sprintf "Error sid_get : Not_found -> id = %i" id
    in Printf.printf "Failwith %s \n" v;
    (Obj.magic "")
    (* failwith v *)


let _serial_kill ()=
  let stop = Bytes.of_string "sk" in
  mpi_send_mfun_to_children stop;
  Printf.printf "\x1b[31mKill pid %i @%s\x1b[0m\n" (pid ()) (hostname ())

let _serial_kill_bcast v =
  let stop = Bytes.of_string "skb" in
  mpi_send_mfun_to_children stop;
  let m_v = Marshal.to_bytes v [Marshal.Closures] in
  mpi_send_mfun_to_children m_v;
  Printf.printf "\x1b[31mKill(T) pid %i @%s\x1b[0m\n" (pid ()) (hostname ())

let _build_tree id_tree id_val =
  Printf.printf "Root is building the final tree\n";
  let stop = Bytes.of_string "b_t" in
  mpi_send_mfun_to_children stop;
  (*Or mpi_send_id_to_children * 2 ? Fastest way ??*)
  let m_v = Marshal.to_bytes (fun _ -> (id_tree,id_val)) [Marshal.Closures] in
  mpi_send_mfun_to_children m_v;
  Printf.printf "\x1b[31mKill(BT) pid %i @%s\x1b[0m\n" (pid ()) (hostname ())
                
let generated_finally ~up ~keep k =
  Printf.printf "Finally : ";
  add_to_stat k keep;
  final_value up keep


(*TESTING/BENCHMARKING*)
(*Counters for closures*)
let nb_closures = ref 0
let total_closures_size = ref 0.0
let total_mashal_time = ref 0.0

let get_closures_info () =
  (!nb_closures,(!total_closures_size,!total_mashal_time))
    
(* let bench_marshal v = *)
(*   incr nb_closures; *)
(*   let m = Marshal.to_bytes v [Marshal.Closures] in *)
(*   total_closures_size := !total_closures_size +. float_of_int (Marshal.total_size m 0); *)
(*   m *)

    
(*warning !! si fonction du genre (fun i -> i ) ====> fun _ _ i -> i et pas l'inverse ;)*)
let generated_replicate f ar =
  let _tmp = replicate (f) in
  (* (\*For comm bench only*\) *)
  (* let start = Unix.gettimeofday () in *)
  (* let mf =  (Marshal.to_bytes f [Marshal.Closures]) in *)
  (* let stop1 = Unix.gettimeofday () in *)
  (* total_mashal_time := !total_mashal_time +. (stop1-.start); *)
  (* total_closures_size := !total_closures_size +. (float_of_int (nb_children())) *. (float_of_int (Marshal.total_size mf 0)); *)
  (* nb_closures := !nb_closures + (nb_children()); *)
  (* (\*EOF*\) *)
  mpi_send_mfun_to_children (Marshal.to_bytes ar [Marshal.Closures]) ;
  (Obj.magic)_tmp

let generated_mkpar f =
  let _tmp = mkpar (f) in
  mpi_send_mfun_to_children (Marshal.to_bytes [||] [Marshal.Closures]);
  (Obj.magic)_tmp

let generated_proj v = proj v

let at t =
  (Obj.magic (sid_get t))

(*Ugly way to handle multifunction with more than 1 argument (max 4)*)
let __multi_call_1 f =
  if root () then
    (fun x ->
       let res = f x in
       _serial_kill_bcast res ; res)
  else
    (fun _ -> daemon () ())

let __multi_call_2 f =
  if root () then
    (fun x y ->
       let res = f x y in
       _serial_kill_bcast res ; res)
  else
    (fun _ _ -> daemon () ())

let __multi_call_3 f =
  if root () then
    (fun x y z ->
       let res = f x y z in
       _serial_kill_bcast res ; res)
  else
    (fun _ _ _ -> daemon () ())

let __multi_call_4 f =
  if root () then
    (fun x y z a ->
       let res = f x y z a in
       _serial_kill_bcast res ; res)
  else
    (fun _ _ _ _ -> daemon () ())

(*Ugly way to handle multifunction with more than 1 argument (max 4)*)
let __multi_tree_call_1 f id_tree =
  if root () then
    (fun x ->
       let res = f x in
      _build_tree (id_tree) (res) ; id_tree)
  else
    (fun _ -> daemon () ())

let __multi_tree_call_2 f id_tree =
  if root () then
    (fun x y ->
       let res = f x y in
      _build_tree (id_tree) (res) ; id_tree)
  else
    (fun _ _-> daemon () ())

let __multi_tree_call_3 f id_tree =
  if root () then
    (fun x y z ->
       let res = f x y z in
      _build_tree (id_tree) (res) ; id_tree)
  else
    (fun _ _ _-> daemon () ())

let __multi_tree_call_4 f id_tree =
  if root () then
    (fun x y z a ->
       let res = f x y z a in
      _build_tree (id_tree) (res) ; id_tree)
  else
    (fun _ _ _ _-> daemon () ())


let __finally v id_tree _ =
  let res = 
    begin
      match fst v with
      | Some x ->
         if root () then(
           (* Printf.printf "End Root (%i)\n%!" (!current_vector_id) *)
           (*For root, the link to the sub-branch is identified by the tree_id (hope it will work …) *)
           add_to_tree_link id_tree (Obj.magic x);(Obj.magic x)
         )
         else(
           (*Printf.printf "End Node (%i)\n%!" (!current_vector_id);*)
           add_to_tree_link (!current_vector_id) (Obj.magic x);(Obj.magic (snd v))
         )
      | None ->
         (*Printf.printf "End leaf (%i)\n%!" (!current_vector_id);*)
         add_to_tree_link (!current_vector_id) (!current_vector_id) ;(Obj.magic (snd v))
    end
  in 
  add_to_tree_tbl id_tree (fun _ -> Obj.magic (snd v));
  res
