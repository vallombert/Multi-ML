open Format

let __debug = ref false
let __debug_mode () =
  __debug := not !__debug

let usage = "usage: " ^ Sys.argv.(0) ^ "[-i string]"

let speclist = [
  (* ("-init", Arg.Set_string infile, ": input Multi-ML file"); *)
  ("-debug",Arg.Unit __debug_mode, "[debug] Debug mode (disabled by default).")
]

let () =
  Arg.parse
    speclist
    (fun x -> raise (Arg.Bad ("Bad argument : " ^ x)))
    usage;;

let read_input_default prompt buffer len =
  output_string Pervasives.stdout prompt; flush Pervasives.stdout;
  let i = ref 0 in
  try
    while true do
      if !i >= len then raise Exit;
      let c = input_char Pervasives.stdin in
      Bytes.set buffer !i c;
      incr i;
      if c = '\n' then raise Exit;
    done;
    (!i, false)
  with
  | End_of_file ->
    (!i, true)
  | Exit ->
    (!i, false)

let first_line = ref true
let got_eof = ref false
let read_interactive_input = ref read_input_default

let refill_lexbuf buffer len =
  if !got_eof then (got_eof := false; 0) else begin
    let prompt =
      if !Clflags.noprompt then ""
      else if !first_line then "" (*#*)
      else if !Clflags.nopromptcont then ""
      else if Lexer.in_comment () then "* "
      else "  "
    in
    first_line := false;
    let (len, eof) = !read_interactive_input prompt buffer len in
    if eof then begin
      Location.echo_eof ();
      if len > 0 then got_eof := true;
      len
    end else
      len
  end

let parse_toplevel_phrase = ref Parse.toplevel_phrase


let d_print color s =
  if !__debug then
    begin
      match color with
      | "debug" -> Printf.printf "\x1b[31m%s\x1b[0m\n" s
      | "info" -> Printf.printf "\x1b[36m%s\x1b[0m\n" s
      | _ -> failwith "unknow color"
    end

exception PPerror

(*La partie sale en attandant un vrai toplevel !*)

let (in_r, in_w) = Unix.pipe ()

(*let (out_r, out_w) = Unix.pipe ()*)

let echo_in = Unix.out_channel_of_descr in_w
(*let echo_out = Unix.in_channel_of_descr out_r*)

(* let pid = Unix.create_process "ocaml" [|"-init" ;"init_seq.ml"|] in_r Unix.stdout Unix.stderr *)

let pid = Unix.create_process "./run.sh" [|""|] in_r Unix.stdout Unix.stdout

(* let init_seq = "#use \"./init_seq.ml\";;\n" *)
(* let _ = Unix.write in_w init_seq 0 (String.length init_seq) *)

let _ = if !__debug then (
  Ast.__debug := true;
  let init_debug = "debug ();;\n" in
  ignore(Unix.write in_w (Bytes.of_string init_debug) 0 (String.length init_debug))
)
  else (
    Ast.__debug := false
  )

(*let _ = Sys.command "clear"*)

let _ = Printf.printf "         Multi-ML version 4.06.1 BETA\n"

let execute_phrase (*print_outcome ppf*) phr =
  begin
    match phr with
    | Parsetree.Ptop_def struc ->
      d_print "debug" "Generate Multi-ML AST …";
      let ast_seq = Ast.transform struc in
      let code = Pprintast_seq.string_of_structure ast_seq in
      d_print "info" "Generated code :";
      d_print "info" code;
      let m = code^";;\n"
      in 
      let _ = Unix.write in_w (Bytes.of_string m) 0 (String.length m) in 
      d_print "debug" "\n\n\n\n\n\n\n\n\n"
    (* #use, #load ... *)
    | Parsetree.Ptop_dir _ -> failwith "TODO #directives"
  end

let loop ppf =
  let lb = Lexing.from_function refill_lexbuf in
  Location.init lb "//toplevel//";
  Location.input_name := "//toplevel//";
  Location.input_lexbuf := Some lb;
  Sys.catch_break true;
  while true do
    try
      Lexing.flush_input lb;
      Location.reset();
      first_line := true;
      let phr = try !parse_toplevel_phrase lb with Exit -> raise PPerror in
      d_print "debug" "Parsing ok";
      ignore(execute_phrase (*true ppf*) phr)
    with
    | End_of_file -> exit 0
    | Sys.Break -> fprintf ppf "Interrupted.@."
    | PPerror -> ()
    | x -> Location.report_exception ppf x
  done


let _ = loop Format.std_formatter
